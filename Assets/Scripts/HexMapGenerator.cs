﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexMapGenerator : MonoBehaviour {
    public bool useFixedSeed;
    public int seed;

   // public bool usePregeneratedTeams = true;

    [Range(0f, 0.5f)]
    public float jitterProbabilty = 0.25f;

    [Range(20f, 200f)]
    public int chunkSizeMin = 10;

    [Range(20f, 200f)]
    public int chunkSizeMax = 100;

    [Range(5, 95)]
    public int landPercentage = 50;

    [Range(1, 5)]
    public int waterLevel = 3;

    [Range(0f, 1f)]
    public float highRiseProbability = 0.25f;

    [Range(0f, 0.4f)]
    public float sinkProbability = 0.2f;

    [Range(-4, 0)]
    public int elevationMin = -2;

    [Range(2, 8)]
    public int elevationMax = 6;

    public HexGrid grid;

    int cellCount;

    HexCellPriorityQueue searchFrontier;

    int searchFrontierPhase;

    public HexMapEditor mapEditor;

    private void RandomiseFeatures(int i)
    {
        int randomNumber = Random.Range(0, 100);
        if (randomNumber <= 50)
        {
            if (randomNumber % 2 == 0)
            {
                mapEditor.SetUrbanLevel(grid.GetCell(i), Random.Range(0, 3));
            }
            if (randomNumber % 3 == 0)
            {
                mapEditor.SetRockLevel(grid.GetCell(i), Random.Range(0, 3));
            }
            //if(randomNumber % 5 == 0)
            //{
            //    mapEditor.SetTreeLevel(grid.GetCell(i), Random.Range(0, 3));
            //}
            mapEditor.SetTreeLevel(grid.GetCell(i), Random.Range(0, 3));

        }
    }

    public void GenerateMap(int x, int z)
    {
        Random.State originalRandomState = Random.state;

        if (!useFixedSeed)
        {
            seed = Random.Range(0, int.MaxValue);
            seed ^= (int)System.DateTime.Now.Ticks;
            seed ^= (int)Time.unscaledTime;
            seed &= int.MaxValue; //forces sign bit to be positive, so no negative seeds
        }

        Random.InitState(seed);

        cellCount = x * z;

        grid.CreateMap(x, z);
        if(searchFrontier == null)
        {
            searchFrontier = new HexCellPriorityQueue();
        }
        for(int i = 0; i < cellCount; i++)
        {
            grid.GetCell(i).WaterLevel = waterLevel;
        }

        CreateLand();

        SetTerrainType();

        for(int i = 0; i < cellCount; i++)
        {
            grid.GetCell(i).SearchPhase = 0;

            RandomiseFeatures(i);

            int numCliffsUp = 0;
            int numCliffsDown = 0;

            foreach (HexCell neighbour in grid.GetCell(i).Neighbours)
            {
                if(neighbour != null && 
                    grid.GetCell(i).GetEdgeType(neighbour) == HexEdgeType.Cliff )
                {
                    if(grid.GetCell(i).Elevation < neighbour.Elevation)
                    {
                        numCliffsUp++;
                    }
                    else
                    {
                        numCliffsDown++;
                    }
                }
            }

            if(numCliffsUp > 2)
            {
                grid.GetCell(i).Elevation++;
            }
            if(numCliffsDown > 3)
            {
                grid.GetCell(i).Elevation--;
            }
        }

        Random.state = originalRandomState;
    }

    private void CreateTeam(int teamNumber)
    {

    }

    private void CreateLand()
    {
        int landBudget = Mathf.RoundToInt(cellCount * landPercentage * 0.01f);

        while(landBudget > 0)
        {
            int chunkSize = UnityEngine.Random.Range(chunkSizeMin, chunkSizeMax - 1);
            if(UnityEngine.Random.value < sinkProbability)
            {
                landBudget = SinkTerrain(chunkSize, landBudget);
            }
            else
            {
                landBudget = RaiseTerrain(chunkSize, landBudget);
            }
        }
    }

    int RaiseTerrain(int chunkSize, int budget)
    {
        searchFrontierPhase += 1;
        HexCell firstCell = GetRandomCell();
        firstCell.SearchPhase = searchFrontierPhase;
        firstCell.DistanceToCell = 0;
        firstCell.SearchHeuristic = 0;
        searchFrontier.Enqueue(firstCell);

        HexCoordinates centre = firstCell.coordinates;

        int rise = UnityEngine.Random.value < highRiseProbability ? 2 : 1;
        int size = 0;

        while (size < chunkSize && searchFrontier.Count > 0)
        {
            HexCell current = searchFrontier.Dequeue();

            int originalElevation = current.Elevation;

            int newElevation = originalElevation + rise;

            if(newElevation > elevationMax)
            {
                continue;
            }

            current.Elevation = newElevation;

            if(originalElevation < waterLevel && newElevation >= waterLevel && --budget == 0)
            {
                break;
            }

            size += 1;

            for(HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                HexCell neighbour = current.GetNeighbour(d);
                if(neighbour &&neighbour.SearchPhase < searchFrontierPhase)
                {
                    neighbour.SearchPhase = searchFrontierPhase;
                    neighbour.DistanceToCell = neighbour.coordinates.DistanceTo(centre);
                    neighbour.SearchHeuristic = UnityEngine.Random.value < jitterProbabilty ? 1:0;
                    searchFrontier.Enqueue(neighbour);
                }
            }
        }
        searchFrontier.Clear();

        return budget;
    }

    int SinkTerrain(int chunkSize, int budget)
    {
        searchFrontierPhase += 1;
        HexCell firstCell = GetRandomCell();
        firstCell.SearchPhase = searchFrontierPhase;
        firstCell.DistanceToCell = 0;
        firstCell.SearchHeuristic = 0;
        searchFrontier.Enqueue(firstCell);

        HexCoordinates centre = firstCell.coordinates;

        int sink = UnityEngine.Random.value < highRiseProbability ? 2 : 1;
        int size = 0;

        while (size < chunkSize && searchFrontier.Count > 0)
        {
            HexCell current = searchFrontier.Dequeue();

            int originalElevation = current.Elevation;

            int newElevation = originalElevation - sink;
            if(newElevation < elevationMin)
            {
                continue;
            }

            current.Elevation = newElevation;

            if (originalElevation >= waterLevel && newElevation < waterLevel)
            {
                budget++;
            }

            size += 1;

            for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                HexCell neighbour = current.GetNeighbour(d);
                if (neighbour && neighbour.SearchPhase < searchFrontierPhase)
                {
                    neighbour.SearchPhase = searchFrontierPhase;
                    neighbour.DistanceToCell = neighbour.coordinates.DistanceTo(centre);
                    neighbour.SearchHeuristic = UnityEngine.Random.value < jitterProbabilty ? 1 : 0;
                    searchFrontier.Enqueue(neighbour);
                }
            }
        }
        searchFrontier.Clear();

        return budget;
    }



    HexCell GetRandomCell()
    {
        return grid.GetCell(UnityEngine.Random.Range(0, cellCount));
    }

    void SetTerrainType()
    {
        for (int i = 0; i < cellCount; i++)
        {
            HexCell cell = grid.GetCell(i);
            if(!cell.IsUnderwater)
            {
                cell.TerrainTypeIndex = cell.Elevation - cell.WaterLevel;
            }
        }
    }
}
