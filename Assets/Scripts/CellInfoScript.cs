﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CellInfoScript : MonoBehaviour {

    public Text cellInfoBox;

    public void UpdateInfoBox(HexCell cell)
    {
        cellInfoBox.text = "Urban move cost: " + cell.UrbanLevel +
            "\nTree move cost: " + cell.TreeLevel +
            "\nRocks move cost: " + cell.RockLevel;

       
    }
}
