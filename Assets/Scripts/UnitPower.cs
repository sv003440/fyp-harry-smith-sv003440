﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPower : MonoBehaviour{
    HexUnit parent;

    public int range;

    public int areaOfEffect;

    public float damage;

    public DamageType typeOfDamage;

    public bool causeStatusEffectResistance;
    public bool causeStatusEffectVulnerability;

    public int timeLimit;

    public float cost;

    //public void InitDefault()
    //{
    //    range = 3;
    //    areaOfEffect = 1;
    //    damage = 1;
    //    typeOfDamage = DamageType.Fire;
    //    causeStatusEffectResistance = true;
    //    causeStatusEffectVulnerability = false;
    //    cost = 2;
    //}
}

