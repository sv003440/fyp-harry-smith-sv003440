﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TurnTrackerText : MonoBehaviour {
    public Text turnTrackerText;
	// Use this for initialization
	void Start () {
        UpdateText();
	}
	
    public void UpdateText()
    {
        turnTrackerText.text = "Player " + GameMode.currentPlayer + "'s go!";
    }
}
