﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HexGameUI : MonoBehaviour {

    public HexGrid grid;

    HexCell currentCell;
    HexCell previousCell;

    HexUnit selectedUnit;
    HexUnit previousUnit;

    //public Text unitInfoBox;
    public InfoBox infoBox;
    public CellInfoScript cellInfo;
    bool isHelpTextEnabled = false;

    public void SetEditMode(bool toggle)
    {
        enabled = !toggle;
        grid.ShowUI(!toggle);
        grid.ClearPath();
    }

    bool UpdateCurrentCell()
    {
        if(currentCell)
        {
            previousCell = currentCell;
        }

        HexCell cell = grid.GetCell();
        if(cell != currentCell)
        {
            currentCell = cell;

            if (previousCell)
            {
                previousCell.DisableHighlight();
                previousUnit = previousCell.Unit;
            }

            return true;
        }

        return false; //no change
    }

    void SelectUnit()
    {
        grid.ClearPath();
        UpdateCurrentCell();

        if(currentCell)
        {
            previousUnit = selectedUnit;
            selectedUnit = currentCell.Unit;

            if (selectedUnit)
            {
                if(selectedUnit.Player == GameMode.currentPlayer)
                {
                    currentCell.EnableHighlight(Color.green, true);
                }
            }
        }
        if (previousUnit)
        {
            previousUnit.Location.EnableHighlight(new Color32(0, 0, 0, 0));//
        }
    }

    private void Update()
    {
        if(EventSystem.current.IsPointerOverGameObject())
        {
            if(Input.GetButtonDown("EnableHelpText"))
            {
                isHelpTextEnabled = !isHelpTextEnabled;
            }

            if(isHelpTextEnabled)
            {
                infoBox.transform.parent.gameObject.SetActive(true);
                cellInfo.transform.parent.gameObject.SetActive(true);

                HexCell cell = grid.GetCell();

                if (cell)
                {
                    cellInfo.UpdateInfoBox(cell);
                    if(cell.Unit)
                    {
                        infoBox.UpdateInfoBox(cell.Unit);
                    }
                    else
                    {
                        infoBox.transform.parent.gameObject.SetActive(false);
                    }
                }
                else
                {
                    infoBox.transform.parent.gameObject.SetActive(false);
                    cellInfo.transform.parent.gameObject.SetActive(false);
                }
            }
            else
            {
                infoBox.transform.parent.gameObject.SetActive(false);
                cellInfo.transform.parent.gameObject.SetActive(false);
            }

            if (Input.GetMouseButtonDown(0))
            {
                SelectUnit();
            }
            else if(selectedUnit)
            {
                if (selectedUnit.Player == GameMode.currentPlayer)
                {
                    if (Input.GetMouseButtonDown(1))
                    {
                        MoveUnit();
                        previousUnit = selectedUnit;
                        selectedUnit = null;
                    }
                    else if (/*Input.GetKey(KeyCode.LeftShift) && */Input.GetButtonDown("UseAbility"))
                    {
                        UseUnitAbilty();
                    }
                    else
                    {
                        DoPathfinding();
                    }
                }
            }
        }
    }

    private void UseUnitAbilty()
    {
        selectedUnit.UseAbilty(grid.GetCell());
    }

    void DoPathfinding()
    {
        if (UpdateCurrentCell())
        {
            if (currentCell)
            {
                if (selectedUnit.IsValidDestination(currentCell))
                {
                    grid.FindPathBetween(selectedUnit.Location, currentCell, selectedUnit);
                    return;
                }
            }

            grid.ClearPath();
        }
    }

    void MoveUnit()
    {
        if(grid.HasPath)
        {
            selectedUnit.Travel(grid.GetPath());
            grid.ClearPath();
        }
    }

    public void EndTurn()
    {
        foreach(HexUnit unit in GameMode.units)
        {
            unit.MoveBudget = unit.MoveBudgetMax;
        }
        GameMode.NextPlayer();
    }
}
