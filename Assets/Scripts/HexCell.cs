﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class HexCell : MonoBehaviour
{
    public HexGridChunk chunk;//the chunk the cell belongs to
    public HexCoordinates coordinates;

    int terrainTypeIndex;

    public HexCell PathFrom { get; set; }
    public int SearchHeuristic { get; set; }
    public HexCell NextWithSamePriority { get; set; }

    public int SearchPhase { get; set; }//0 == cell not reached yet, 1 == cell is in frontiet, 2 == taken out of the frontier

    public int SearchPriority
    {
        get
        {
            return distanceToCell + SearchHeuristic;
        }
    }

    public HexUnit Unit { get; set; }

    public int TerrainTypeIndex
    {
        get
        {
            return terrainTypeIndex;
        }
        set
        {
            if(terrainTypeIndex != value)
            {
                terrainTypeIndex = value;
                Refresh();
            }
        }
    }

    private int elevation = int.MinValue; //will get initialised to 0 during startup
    public int Elevation
    {
        get
        {
            return elevation;
        }
        set
        {
            if (elevation == value)
            {
                return; //no change
            }
            elevation = value;

            RefreshPosition();

            ValidateRivers();

            for (int i = 0; i < roads.Length; i++)
            {
                if (roads[i] && GetElevationDifference((HexDirection)i) > 1)
                {
                    SetRoad(i, false);
                }
            }

            Refresh();
        }
    }

    private void RefreshPosition()
    {
        Vector3 position = transform.localPosition;
        position.y = elevation * HexMetrics.elevationStep;
        position.y += (HexMetrics.SampleNoise(position).y * 2f - 1f) * HexMetrics.elevationPerturbStrength;
        transform.localPosition = position;

        Vector3 uiPosition = uiRect.localPosition;
        uiPosition.z = -position.y;
        uiRect.localPosition = uiPosition;
    }

    public float StreamBedY
    {
        get
        {
            return (elevation + HexMetrics.streamBedElevationOffset) * HexMetrics.elevationStep;
        }
    }

    public float RiverSurfaceY
    {
        get
        {
            return (elevation + HexMetrics.waterElevationOffset) * HexMetrics.elevationStep;
        }
    }

    public float WaterSurfaceY
    {
        get
        {
            return (waterLevel + HexMetrics.waterElevationOffset) * HexMetrics.elevationStep;
        }
    }
    public Vector3 Position
    {
        get
        {
            return transform.localPosition;
        }
    }

    bool hasIncomingRiver, hasOutgoingRiver;

    public bool HasIncomingRiver
    {
        get
        {
            return hasIncomingRiver;
        }
    }
    public bool HasOutgoingRiver
    {
        get
        {
            return hasOutgoingRiver;
        }
    }

    HexDirection incomingRiverDirection, outgoingRiverDirection;

    public HexDirection IncomingRiver
    {
        get
        {
            return incomingRiverDirection;
        }
    }
    public HexDirection OutgoingRiver
    {
        get
        {
            return outgoingRiverDirection;
        }
    }

    public bool HasRiver
    {
        get
        {
            return hasOutgoingRiver || hasIncomingRiver;
        }
    }

    public bool HasRiverBeginOrEnd
    {
        get
        {
            return hasIncomingRiver != hasOutgoingRiver;
        }
    }

    public HexDirection RiverBeginOrEndDirection
    {
        get
        {
            return hasIncomingRiver ? incomingRiverDirection : outgoingRiverDirection;
        }
    }

    public bool HasRiverThroughEdge(HexDirection direction)
    {
        return (hasIncomingRiver && (incomingRiverDirection == direction)) || (hasOutgoingRiver && (outgoingRiverDirection == direction));
    }

    private int waterLevel;
    public int WaterLevel
    {
        get
        {
            return waterLevel;
        }
        set
        {
            if(waterLevel == value)
            {
                return;
            }
            waterLevel = value;
            ValidateRivers();
            Refresh();
        }
    }

    public bool IsUnderwater
    {
        get
        {
            return waterLevel > elevation;
        }
    }

    int urbanLevel;

    public int UrbanLevel
    {
        get
        {
            return urbanLevel;
        }
        set
        {
            if(urbanLevel !=value)
            {
                urbanLevel = value;
                RefreshSelfOnly();
            }
        }
    }

    int treeLevel;
    public int TreeLevel
    {
        get
        {
            return treeLevel;
        }
        set
        {
            if(treeLevel != value)
            {
                treeLevel = value;
                RefreshSelfOnly();
            }
        }
    }

    int rockLevel;
    public int RockLevel
    {
        get
        {
            return rockLevel;
        }
        set
        {
            if (rockLevel != value)
            {
                rockLevel = value;
                RefreshSelfOnly();
            }
        }
    }

    bool walled;

    public bool Walled
    {
        get
        {
            return walled;
        }
        set
        {
            if(walled!=value)
            {
                walled = value;
                Refresh();
            }
        }
    }

    bool IsValidRiverDestination(HexCell neighbour)
    {
        return neighbour && (elevation >= neighbour.elevation || waterLevel == neighbour.elevation);
    }

    int distanceToCell;

    public int DistanceToCell
    {
        get
        {
            return distanceToCell;
        }
        set
        {
            distanceToCell = value;
        }
    }

    public RectTransform uiRect;

    [SerializeField] //serialize field allows the editor to see the variable without making it publicly accessible by other scripts etc
    HexCell[] neighbours;

    public HexCell[] Neighbours
    {
        get
        {
            return neighbours;
        }
    }

    [SerializeField]
    bool[] roads;

    public bool HasRoadThroughEdge(HexDirection direction)
    {
        return roads[(int)direction];
    }

    public bool HasRoads
    {
        get
        {
            for (int i = 0; i< roads.Length; i++)
            {
                if(roads[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    public void RemoveRoads()
    {
        for(int i = 0; i < neighbours.Length; i++)
        {
            if(roads[i])
            {
                SetRoad(i, false);
            }
        }
    }

    public void AddRoad(HexDirection direction)
    {
        if (!roads[(int)direction] && !HasRiverThroughEdge(direction) && GetElevationDifference(direction) <= 1)
        {
            SetRoad((int)direction, true);
        }
    }

    private void SetRoad(int index, bool state)
    {
        roads[index] = state;
        neighbours[index].roads[(int)((HexDirection)index).Opposite()] = state;
        neighbours[index].RefreshSelfOnly();
        RefreshSelfOnly();
    }

    public int GetElevationDifference(HexDirection direction)
    {
        int difference = elevation - GetNeighbour(direction).elevation;
        return difference >= 0 ? difference : -difference; //why not Mathf.Abs()?
    }

    public HexCell GetNeighbour(HexDirection direction)
    {
        return neighbours[(int)direction];
    }

    public void SetNeighbour(HexDirection direction, HexCell cell)
    {
        neighbours[(int)direction] = cell;
        cell.neighbours[(int)direction.Opposite()] = this;//this is an extension method
    }

    /*From: https://catlikecoding.com/unity/tutorials/hex-map/part-2/
        * An extension method is a static method inside a static class that behaves like an instance method of some type. 
        * That type could be anything, a class, an interface, a struct, a primitive value, or an enum. 
        * The first argument of an extension method needs to have the this keyword. 
        * It defines the type and instance value that the method will operate on.

    Does this allow us to add methods to everything? 
    Yes, just like you could write any static method that has any type as its argument. 
    Is this a good idea? When used in moderation, it can be. It is a tool that has its uses, but wielding it with abandon will produce an unstructured mess.

    */

    public HexEdgeType GetEdgeType(HexDirection direction)
    {
        return HexMetrics.GetEdgeType(elevation, neighbours[(int)direction].elevation); //check that there is a neighbour?
    }

    public HexEdgeType GetEdgeType(HexCell otherCell)
    {
        return HexMetrics.GetEdgeType(elevation, otherCell.Elevation); //could be any cell but we should be able to limit it to neighbours ourselves
    }

    void Refresh()
    {
        if(chunk)
        {
            chunk.Refresh();
            for(int i = 0; i < neighbours.Length; i++)
            {
                HexCell neighbour = neighbours[i];
                if(neighbour != null && neighbour.chunk != chunk)
                {
                    neighbour.chunk.Refresh();
                }
            }
            if(Unit)
            {
                Unit.ValidateLocation();
            }
        }
    }

    void RefreshSelfOnly()
    {
        if (chunk)
        {
            chunk.Refresh();
        }
        if (Unit)
        {
            Unit.ValidateLocation(); //may not be needed, after terrain gen it shouldn't move so there shouldn't be any problems with a units location
        }
    }

    public void RemoveOutgoingRiver()
    {
        if(!hasOutgoingRiver)
        {
            return;
        }

        hasOutgoingRiver = false;
        RefreshSelfOnly();

        HexCell neighbour = GetNeighbour(outgoingRiverDirection);
        neighbour.hasIncomingRiver = false;
        neighbour.RefreshSelfOnly();
    }

    public void RemoveIncomingRiver()
    {
        if (!hasIncomingRiver)
        {
            return;
        }

        hasIncomingRiver = false;
        RefreshSelfOnly();

        HexCell neighbour = GetNeighbour(outgoingRiverDirection);
        neighbour.hasOutgoingRiver = false;
        neighbour.RefreshSelfOnly();
    }
        
    public void RemoveRiver()
    {
        RemoveOutgoingRiver();
        RemoveIncomingRiver();
    }

    public void SetOutgoingRiver(HexDirection direction)
    {
        if(hasOutgoingRiver && outgoingRiverDirection == direction)
        {
            return;
        }

        HexCell neighbour = GetNeighbour(direction);

        if(!IsValidRiverDestination(neighbour))
        {
            return;
        }

        RemoveOutgoingRiver();
        if(hasIncomingRiver && incomingRiverDirection == direction)
        {
            RemoveIncomingRiver();
        }

        hasOutgoingRiver = true;
        outgoingRiverDirection = direction;


        neighbour.RemoveIncomingRiver();
        neighbour.hasIncomingRiver = true;
        neighbour.incomingRiverDirection = direction.Opposite();

        SetRoad(direction.AsInt(), false);
    }

    void ValidateRivers()
    {
        if(hasOutgoingRiver && !IsValidRiverDestination(GetNeighbour(outgoingRiverDirection)))
        {
            RemoveOutgoingRiver();
        }

        if(hasIncomingRiver && !GetNeighbour(incomingRiverDirection).IsValidRiverDestination(this))
        {
            RemoveIncomingRiver();
        }
    }

    public void Save(BinaryWriter writer)
    {
        writer.Write((byte)terrainTypeIndex);
        writer.Write((byte)(elevation + 127)); //this gives us an elevation range of [-127,128] - remember to unpack when loading
        writer.Write((byte)waterLevel);
        writer.Write((byte)urbanLevel);
        writer.Write((byte)treeLevel);
        writer.Write((byte)rockLevel);
        writer.Write(walled);

        if(hasIncomingRiver)
        {
            writer.Write((byte)(incomingRiverDirection + 128));
        }
        else
        {
            writer.Write((byte)0);
        }

        if(hasOutgoingRiver)
        {
            writer.Write((byte)(outgoingRiverDirection + 128));
        }
        else
        {
            writer.Write((byte)0);
        }

        int roadFlags = 0;
        for (int i = 0; i < roads.Length; i++)
        {
            if(roads[i])
            {
                roadFlags |= 1 << i;
            }
        }
        writer.Write((byte)roadFlags);
    }

    public void Load(BinaryReader reader, int header)
    {
        terrainTypeIndex = reader.ReadByte();
        elevation = reader.ReadByte();
        if(header >= 3)
        {
            elevation -= 127;
        }
        RefreshPosition();
        waterLevel = reader.ReadByte();
        urbanLevel = reader.ReadByte();
        treeLevel = reader.ReadByte();
        rockLevel = reader.ReadByte();

        walled = reader.ReadBoolean();

        byte riverData = reader.ReadByte();
        if(riverData >= 128)
        {
            hasIncomingRiver = true;
            incomingRiverDirection = (HexDirection)(riverData - 128);
        }
        else
        {
            hasIncomingRiver = false;
        }

        riverData = reader.ReadByte();
        if (riverData >= 128)
        {
            hasOutgoingRiver = true;
            outgoingRiverDirection = (HexDirection)(riverData - 128);
        }
        else
        {
            hasOutgoingRiver = false;
        }

        int roadFlags = reader.ReadByte();
        for (int i=0; i< roads.Length; i++)
        {
            roads[i] = (roadFlags & (1<<i)) != 0;
        }
    }

    public void DisableHighlight()
    {
        if(!Unit)
        {
            Image highlight = uiRect.GetChild(0).GetComponent<Image>();
            highlight.enabled = false;
        }
    }

    public void EnableHighlight(Color colour, bool isOverride = false)
    {
        Image highlight = uiRect.GetChild(0).GetComponent<Image>();
        if(Unit && !isOverride)
        {
            highlight.color = Unit.Player == 1 ? Color.blue : Color.black;
        }
        else
        {
            highlight.color = colour;
        }
        highlight.enabled = true;
    }

    public void SetLabel(string text)
    {
        UnityEngine.UI.Text label = uiRect.GetComponent<Text>();
        label.text = text;
    }
}

