﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BudgetTrackingText : MonoBehaviour {

    public Text budgetTrackingText;

    public TeamBuilder teamBuilder;

    // Use this for initialization
    void Start () {
        budgetTrackingText.text = "Both players start with a budget of " + teamBuilder.budgetMax;

    }
	
	public void UpdateText () {
        budgetTrackingText.text = "Player " + teamBuilder.player + "'s current budget is " + teamBuilder.teamBudgets[teamBuilder.player-1];
    }
}
