﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TeamBuilder : UIBase {

    //List<HexUnit> team;
    public HexGrid hexGrid;

    public int player = 1;
    // Use this for initialization

    public float budgetMax = 30f;
    //float[] teamBudgetsMax;// = {30f,30f};
    public float[] teamBudgets;

    void Start ()
    {
        Reset();
        //teamBudgets = Enumerable.Repeat(budgetMax, GameMode.playerCount).ToArray();
    }

    public void AddUnitToList( HexUnit unit)
    {
        if(unit.cost > teamBudgets[player - 1])
        {
            Debug.Log("Budget left: " + teamBudgets[player - 1] + " That unit costs: " + unit.cost);
            return;
        }

        hexGrid.AddUnit(Instantiate(unit.unitPrefab), player, new HashSet<DamageType>(), new HashSet<DamageType>());
        teamBudgets[player - 1] -= unit.cost;
    }

    public void SetPlayer(int playerNumber)
    {
        player = playerNumber;
    }

    public void Reset()
    {
        player = 1;
        teamBudgets = Enumerable.Repeat(budgetMax, GameMode.playerCount).ToArray();
        GameMode.Reset();
    }
}
