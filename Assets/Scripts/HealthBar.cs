﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

    MaterialPropertyBlock matBlock;
    MeshRenderer meshRenderer;
    HexUnit parent;

    private void AlignToCamera()
    {
        if(HexMapCamera.instance != null)
        {
            Transform cameraTransform = HexMapCamera.instance.stick;
            Vector3 forward = transform.position - cameraTransform.position;

            forward.Normalize();
            Vector3 up = Vector3.Cross(cameraTransform.right, forward);
            transform.rotation = Quaternion.LookRotation(forward, up);
        }
    }

    void UpdateParameters()
    {
        meshRenderer.GetPropertyBlock(matBlock);
        matBlock.SetFloat("_Fill", parent.Health / parent.maxHealth);
        meshRenderer.SetPropertyBlock(matBlock);
    }
    // Use this for initialization
    void Start () {

		
	}

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        matBlock = new MaterialPropertyBlock();
        parent = transform.parent.gameObject.GetComponent<HexUnit>();
        meshRenderer.enabled = true;
    }

    // Update is called once per frame
    void Update ()
    {
        AlignToCamera();
        UpdateParameters();
	}
}
