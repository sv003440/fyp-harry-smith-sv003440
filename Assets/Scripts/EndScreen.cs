﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScreen : UIBase {
    public void LoadGame()
    {
        SceneManager.LoadScene("Fantasy Tactics");
    }

}
