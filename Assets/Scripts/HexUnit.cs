﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class HexUnit : MonoBehaviour {

    public HexUnit unitPrefab;
    public static HexUnit unitPrefabDefault;

    public string unitType;

    public float cost;

    public float visualTravelSpeed = 4f; //probably make this configurable, civ does it. Civ also has the option of teleporting units (I think)
    const float rotationSpeed = 180f; //again some people may want this switched off

    public int Player { get; set; }

    public AudioSource soundSource;

    private void Start()
    {
        currentHealth = maxHealth;
        moveBudget = moveBudgetMax;
    }

    public int GetTurnsToTravel(int distance)
    {
        return moveBudget <= 0 ? 10 : (distance) / moveBudget;
    }

    public int GetMoveCost(HexCell fromCell, HexCell toCell, HexDirection direction)
    {
        HexEdgeType edgeType = fromCell.GetEdgeType(toCell);

        if (edgeType == HexEdgeType.Cliff && !CanTravelOverWater)
        {
            return -1; //no movement possible
        }

        int moveCost;

        if (fromCell.HasRoadThroughEdge(direction))
        {
            moveCost = 1;
        }
        else if (fromCell.Walled != toCell.Walled)
        {
            return -1; //no movement possible
        }
        else
        {
            moveCost = edgeType == HexEdgeType.Flat ? 5 : 10;
            moveCost += toCell.TreeLevel + toCell.RockLevel + toCell.UrbanLevel;
        }

        return moveCost;
    }
    public bool CanTravelOverWater = false;

    [SerializeField]
    int moveBudgetMax = 24;
    public int MoveBudgetMax
    {
        get
        {
            return moveBudgetMax;
        }
    }
    int moveBudget;
    public int MoveBudget
    {
        get
        {
            return moveBudget;
        }
        set
        {
            moveBudget = value;
        }
    }
    public UnitPower ability;
    public UnitPower Ability
    {
        get
        {
            return ability;
        }
        set
        {
            ability = value;
        }
    }

    HashSet<DamageType> vulnerabilties;
    public HashSet<DamageType> Vulnerabilities
    {
        get
        {
            return vulnerabilties;
        }
        set
        {
            vulnerabilties = value;
        }
    }

    HashSet<DamageType> resistances;
    public HashSet<DamageType> Resistances
    {
        get
        {
            return resistances;
        }
        set
        {
            resistances = value;
        }
    }

    public float maxHealth;
    float currentHealth;
    public float Health
    {
        get
        {
            return currentHealth;
        }
        set
        {
            currentHealth = value;
            //Debug.Log("Current health is " + currentHealth);
            if(currentHealth <= 0)
            {
                Die();
            }
        }
    }

    HexCell location;
    public HexCell Location
    {
        get
        {
            return location;
        }
        set
        {
            if(location)
            {
                location.Unit = null;
            }
            location = value;
            value.Unit = this;
            transform.localPosition = value.Position + new Vector3(0.0f, CanTravelOverWater ? value.WaterLevel : 0.0f, 0.0f); //so that fliers will not sink into water
            location.EnableHighlight(Color.black);
        }
    }

    float orientation;
    public float Orientation
    {
        get
        {
            return orientation;
        }
        set
        {
            orientation = value;
            transform.localRotation = Quaternion.Euler(0f, value, 0f);
        }
    }
    
    public void ValidateLocation()
    {
        transform.localPosition = location.Position;
    }

    public void Die()
    {
        if (location)
        {
            location.Unit = null;
        }
        Destroy(gameObject);
        
        GameMode.CheckForWinCondition();
    }

    public void DieWithoutCheckingForWin()
    {
        if (location)
        {
            location.Unit = null;
        }
        Destroy(gameObject);
    }

    public void Save(BinaryWriter writer)
    {
        location.coordinates.Save(writer);
        writer.Write(orientation);
        writer.Write(Player);
    }

    //public static void Load(BinaryReader reader, HexGrid grid, int versionNumber)
    //{
    //    HexCoordinates coordinates = HexCoordinates.Load(reader);
    //    float orientation = reader.ReadSingle();
    //    int player = 0; //just for backwards compatibility, unlikely to be necessary
    //    if (versionNumber >= 4)
    //    {
    //        player = reader.ReadInt32();//TODO: optimise this
    //    }
    //    grid.AddUnit(Instantiate(unitPrefab), grid.GetCell(coordinates), orientation, player);
    //}

    private void Awake()
    {
        currentHealth = maxHealth;
    }

    public bool IsValidDestination(HexCell cell)
    {
        return cell != null && (!cell.IsUnderwater || CanTravelOverWater) && !cell.Unit;
    }

    public void Travel(List<HexCell> path)
    {
        soundSource.Play();
        Location = path[path.Count - 1];
       // pathToTravel = path;

        if(visualTravelSpeed > 0)
        {
            StopAllCoroutines();
            StartCoroutine(TravelPath(path));
        }
        else //travel speed is 0 or lower, teleport to destination
        {
            ValidateLocation();
        }
    }

    IEnumerator TravelPath(List<HexCell> pathToTravel)
    {
        Vector3 a, b, c = pathToTravel[0].Position;
        transform.localPosition = c;
        yield return LookAt(pathToTravel[1].Position);

        float t = Time.deltaTime * visualTravelSpeed;

        for (int i = 1; i < pathToTravel.Count; i++)
        {
            a = c;

            b = pathToTravel[i - 1].Position;
            c = (b + pathToTravel[i].Position) * 0.5f;//averaging the positions of cells to get edge positions

            for (; t < 1f; t += Time.deltaTime * visualTravelSpeed)
            {
                transform.localPosition = Bezier.GetPoint(a, b, c, t);

                Vector3 d = Bezier.GetDerivative(a, b, c, t);
                d.y = 0f;

                transform.localRotation = Quaternion.LookRotation(d);//the derivative of a quadratic bezier gives the travel direction

                yield return null;
            }

            t -= 1f; //to prevent timeloss (only noticeable at low framerates) we transfer some time over
        }

        a = c;

        b = pathToTravel[pathToTravel.Count - 1].Position;//travel to middle of destination cell

        c = b;

        for (; t < 1f; t += Time.deltaTime * visualTravelSpeed)
        {
            transform.localPosition = Bezier.GetPoint(a, b, c, t);

            Vector3 d = Bezier.GetDerivative(a, b, c, t);
            d.y = 0f;
            transform.localRotation = Quaternion.LookRotation(d);//the derivative of a quadratic bezier gives the travel direction

            yield return null;
        }

        transform.localPosition = location.Position; //at low framerates the unit stops at a noticeable distance from it's intended end point
        orientation = transform.localRotation.eulerAngles.y;

        for (int i = 0; i < pathToTravel.Count - 1; i++)
        {
            moveBudget -= GetMoveCost(pathToTravel[i], pathToTravel[i + 1], HexDirection.E); //HexDirection is only
                                                                                             // used here to check for roads, which are not currently being used
        }

        ListPool<HexCell>.Add(pathToTravel);
    }

    void OnEnable()
    {
        if(location)
        {
            transform.localPosition = location.Position; //coroutines don't survive recompile so if recompile happens for some reason during a move the unit might be in the wrong place
        }
    }

    IEnumerator LookAt(Vector3 point)
    {
        point.y = transform.localPosition.y;

        Quaternion fromRotation = transform.localRotation;
        Quaternion toRotation = Quaternion.LookRotation(point - transform.localPosition);

        float angle = Quaternion.Angle(fromRotation, toRotation);
        float speed = rotationSpeed / angle;

        if (angle > 0)
        {
            for (float t = Time.deltaTime * speed; t < 1f; t += Time.deltaTime * speed)
            {
                transform.localRotation = Quaternion.Slerp(fromRotation, toRotation, t);
                yield return null;
            }
        }

        transform.LookAt(point);
        orientation = transform.localRotation.eulerAngles.y;
    }

    public void UseAbilty(HexCell targetCell)
    {
        if(GameMode.CurrentTurnBudget < 0 || GameMode.CurrentTurnBudget < ability.cost)
        {
            Debug.Log("Spent too much of turn budget to do that");
            return;
        }

        if (location.coordinates.DistanceTo(targetCell.coordinates) > ability.range)
        {
            Debug.Log("target cell is too far away");
            return;
        }

        if (!targetCell.Unit)
        {
            Debug.Log("no enemy unit to target");
            return;
        }

        HexUnit targetUnit = targetCell.Unit;

        if(targetUnit.Player == this.Player)
        {
            Debug.Log("Can't target friendlies");
            return;
        }

        if(ability.causeStatusEffectResistance == true)
        {
            targetUnit.AddResistance(ability.typeOfDamage);
        }
        else if (ability.causeStatusEffectVulnerability == true)
        {
            targetUnit.AddVulnerabilty(ability.typeOfDamage);
        }

        targetUnit.TakeDamage(ability); //the order of this and the resistance/vulnerability matters, I'll think about it some more
        GameMode.CurrentTurnBudget -= ability.cost;
    }

    public void TakeDamage(float damage, DamageType damageType)
    {
        if(resistances.Contains(damageType))
        {
            damage /= 2;
        }
        else if (vulnerabilties.Contains(damageType))
        {
            damage *= 2;
        }

        Health -= damage;
    }

    public void TakeDamage(UnitPower power)
    {
        TakeDamage(power.damage, power.typeOfDamage);
    }

    public void AddResistance(DamageType statusEffect)
    {
        if (vulnerabilties.Contains(statusEffect))
        {
           if( vulnerabilties.Remove(statusEffect))
            {
                Debug.Log("Causing vulnerability to " + statusEffect.ToString());
            }
        }
        else
        {
            if(resistances.Add(statusEffect))
            {
                Debug.Log("Causing resistance to " + statusEffect.ToString());
            }
            else
            {
                Debug.Log("Resistance to " + statusEffect.ToString() + "already in hashset");
            }
        }
    }

    public void AddVulnerabilty(DamageType statusEffect)
    {
        if (resistances.Contains(statusEffect)) //can be written more verbosely because HasSet<>.Remove returns false if statusEffect not contained but I like that this is clearer
        {                                       //if performance is an issue it could be changed
            resistances.Remove(statusEffect);
        }
        else
        {
            vulnerabilties.Add(statusEffect);
        }
    }
}
