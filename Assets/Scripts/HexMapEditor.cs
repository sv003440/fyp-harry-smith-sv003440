﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.IO;


public class HexMapEditor : MonoBehaviour
{
    public HexGrid hexGrid;

    int activeElevation;
    int activeWaterLevel;

    bool applyElevation = true;
    bool applyWaterLevel = true;

    int brushSize;

    int activeUrbanLevel, activeTreeLevel, activeRockLevel;
    bool applyUrbanLevel, applyTreeLevel, applyRockLevel;

    public Material terrainMaterial;

    enum OptionalToggle
    {
        Ignore, Yes, No
    }

    OptionalToggle riverMode, roadMode, walledMode;

    bool isDrag;
    HexDirection dragDirection;
    HexCell previousCell;//, searchFromCell, searchToCell;

    int activeTerrainTypeIndex = -1;

    private void Awake()
    {
        terrainMaterial.DisableKeyword("GRID_ON");
        SetEditMode(false);
    }

    public void SetTerrainTypeIndex(int index)
    {
        activeTerrainTypeIndex = index;
    }

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                Debug.Log(GetCellUnderCursor().coordinates.ToString());
            }
            if (Input.GetMouseButton(0))
            {
                HandleInput();
                return;
            }
            if (Input.GetKeyDown(KeyCode.U))
            {
                if(Input.GetKey(KeyCode.LeftShift))
                {
                    CreateUnit(2);
                }
                else
                {
                    CreateUnit(1);
                }
                return;
            }
        }
        previousCell = null;
    }

    HexCell GetCellUnderCursor()
    {
        return hexGrid.GetCell(Camera.main.ScreenPointToRay(Input.mousePosition));
    }

    void HandleInput()
    {
        HexCell currentCell = GetCellUnderCursor();

        if (currentCell)
        {
            if(previousCell && previousCell != currentCell)
            {
                ValidateDrag(currentCell);
            }
            else
            {
                isDrag = false;
            }

            EditCells(currentCell);
            
            previousCell = currentCell;
//            isDrag = false;
        }
        else
        {
            previousCell = null;
        }

    }

    private void ValidateDrag(HexCell currentCell)
    {
        for(dragDirection = HexDirection.NE; dragDirection <= HexDirection.NW; dragDirection++)
        {
            if(previousCell.GetNeighbour(dragDirection) == currentCell)
            {
                isDrag = true;
                return;
            }
        }

        isDrag = false;
    }

    void EditCells(HexCell centre)
    {
        /*
            * The first cell of the bottom row has the same X coordinate as the center cell. 
            * This coordinate decreases as the row number increases.
            * The last cell always has an X coordinate equal to the center's plus the radius.
            */
        int centreX = centre.coordinates.X;
        int centreZ = centre.coordinates.Z;

        for (int r = 0, z = centreZ - brushSize; z <= centreZ; z++, r++)
        {
            for (int x = centreX - r; x <= centreX + brushSize; x++)
            {
                EditCell(hexGrid.GetCell(new HexCoordinates(x, z)));
            }
        }
        for (int r = 0, z = centreZ + brushSize; z > centreZ; z--, r++)
        {
            for (int x = centreX - brushSize; x <= centreX + r; x++)
            {
                EditCell(hexGrid.GetCell(new HexCoordinates(x, z)));
            }
        }
    }

    public void SetUrbanLevel(HexCell cell, int urbanLevel)
    {
        cell.UrbanLevel = urbanLevel;
    }

    public void SetTreeLevel(HexCell cell, int treeLevel)
    {
        cell.TreeLevel = treeLevel;
    }

    public void SetRockLevel(HexCell cell, int rockLevel)
    {
        cell.RockLevel = rockLevel;
    }

    void EditCell(HexCell cell)
    {
        if (cell)
        {
            if(activeTerrainTypeIndex>=0)
            {
                cell.TerrainTypeIndex = activeTerrainTypeIndex;
            }
            if (applyElevation)
            {
                cell.Elevation = activeElevation;
            }
            if(applyWaterLevel)
            {
                cell.WaterLevel = activeWaterLevel;
            }
            if(applyUrbanLevel)
            {
                SetUrbanLevel(cell, activeUrbanLevel);

            }
            if (applyTreeLevel)
            {
                SetTreeLevel(cell, activeTreeLevel);
            }
            if (applyRockLevel)
            {
                SetRockLevel(cell, activeRockLevel);

            }
            if (riverMode == OptionalToggle.No)
            {
                cell.RemoveRiver();
            }
            if(roadMode == OptionalToggle.No)
            {
                cell.RemoveRoads();
            }
            if(walledMode != OptionalToggle.Ignore)
            {
                cell.Walled = walledMode == OptionalToggle.Yes;
            }
            else if(isDrag)
            {
                HexCell otherCell = cell.GetNeighbour(dragDirection.Opposite());
                if(otherCell)
                {
                    if(riverMode == OptionalToggle.Yes)
                    {
                        otherCell.SetOutgoingRiver(dragDirection);
                    }
                    if(roadMode == OptionalToggle.Yes)
                    {
                        otherCell.AddRoad(dragDirection);
                    }
                }
            }
        }
    }

    public void SetElevation(float elevation)
    {
        activeElevation = (int)elevation;
    }

    public void SetAppyElevation(bool toggle)
    {
        applyElevation = toggle;
    }

    public void SetBrushSize(float size)
    {
        brushSize = (int)size;
    }

    //public void ShowUI(bool visible)
    //{
    //    hexGrid.ShowUI(visible);
    //}

    public void SetRiverMode(int mode)
    {
        riverMode = (OptionalToggle)mode;
    }
    public void SetRoadMode(int mode)
    {
        roadMode = (OptionalToggle)mode;
    }

    public void SetApplyWaterLevel(bool toggle)
    {
        applyWaterLevel = toggle;
    }

    public void SetWaterLevel(float level)
    {
        activeWaterLevel = (int)level;
    }

    public void SetApplyUrbanLevel(bool toggle)
    {
        applyUrbanLevel = toggle;
    }

    public void SetUrbanLevel(float level)
    {
        activeUrbanLevel = (int)level;
    }

    public void SetApplyTreeLevel(bool toggle)
    {
        applyTreeLevel = toggle;
    }

    public void SetTreeLevel(float level)
    {
        activeTreeLevel = (int)level;
    }

    public void SetApplyRockLevel(bool toggle)
    {
        applyRockLevel = toggle;
    }

    public void SetRockLevel(float level)
    {
        activeRockLevel = (int)level;
    }

    public void SetWalledMode(int mode)
    {
        walledMode = (OptionalToggle)mode;
    }

    public void ShowGrid(bool visible)
    {
        if(visible)
        {
            terrainMaterial.EnableKeyword("GRID_ON");
        }
        else
        {
            terrainMaterial.DisableKeyword("GRID_ON");
        }
    }

    public void SetEditMode(bool toggle)
    {
        enabled = toggle;
    }

    void CreateUnit(int player, HexCell location = null)
    {
        HexCell cell = location == null ? GetCellUnderCursor() : location;

        if (cell && !cell.Unit &&!cell.IsUnderwater)
        {
            float tempOrientation = UnityEngine.Random.Range(0f, 360f);//temp for testing, I don't think random orientation will be good for real game

            hexGrid.AddUnit(Instantiate(HexUnit.unitPrefabDefault), cell, tempOrientation, player, /*new UnitPower(),*/ new HashSet<DamageType>(), new HashSet<DamageType>());
        }
    }

    void DestroyUnit()
    {
        Debug.Log("Destroy unit function not currently in use, ");
        //HexCell cell = GetCellUnderCursor();
        //if(cell && cell.Unit)
        //{
        //    hexGrid.RemoveUnit(cell.Unit);
        //}
    }
}
