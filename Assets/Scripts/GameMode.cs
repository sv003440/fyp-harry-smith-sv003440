﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.SceneManagement;

public static class GameMode
{
    public static int currentPlayer = 1;

    public static int playerCount = 2;

    public const float maxTurnBudget = 12;

    static float currentTurnBudget = maxTurnBudget;

    public static List<HexUnit> units = new List<HexUnit>();

    public static void Reset()
    {
        currentPlayer = 1;
        currentTurnBudget = maxTurnBudget;
        ClearUnits();
    }

    public static float CurrentTurnBudget
    {
        get
        {
            return currentTurnBudget;
        }
        set
        {
            currentTurnBudget = value;
            //if(currentTurnBudget <= 0) //ALSO NEEDS TO UPDATE THE UI
            //{
            //    NextPlayer();
            //}
        }
    }

    public static void NextPlayer()
    {
        if(currentPlayer == playerCount)
        {
            currentPlayer = 1;
        }
        else
        {
            currentPlayer++;
        }

        currentTurnBudget = maxTurnBudget;
    }

    public static void ClearUnits()
    {
        for (int i = 0; i < units.Count; i++)
        {
            if(units[i] != null)
            {
                units[i].DieWithoutCheckingForWin();
            }
        }

        units.Clear();
    }

    public static void CheckForWinCondition()
    {
        var count = units.Count(unit => unit != null && unit.Player != currentPlayer);

        if(count <= 1) //because of the order of when things get destroyed I think
        {
            SceneManager.LoadScene("EndScreen");
        }

    }

    public static void Load(BinaryReader reader, int header)
    {
        playerCount = reader.ReadInt32(); //TODO: Optimise this
        currentPlayer = reader.ReadInt32();
    }

    public static void Save(BinaryWriter writer)
    {
        writer.Write(playerCount);
        writer.Write(currentPlayer);
    }
}


