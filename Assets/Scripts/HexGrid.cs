﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class HexGrid : MonoBehaviour
{
    public int cellCountX = 20, cellCountZ = 15;
    int chunkCountX, chunkCountZ;

    public HexCell cellPrefab;
    HexCell currentPathFrom, currentPathTo;
    bool currentPathExists;

    public HexGridChunk chunkPrefab;

    HexGridChunk[] chunks;
    HexCell[] cells;

    public Text cellLabelPrefab;

    public Texture2D noiseSource;

    public int seed;

    HexCellPriorityQueue searchFrontier;
    int searchFrontierPhase;

    public HexUnit unitPrefab;

    public bool HasPath
    {
        get
        {
            return currentPathExists;
        }
    }

    private void OnEnable()
    {
        if(!HexMetrics.noiseSource)
        {
            HexMetrics.noiseSource = noiseSource; //reassigned here to survive recompilation
            HexMetrics.InitialiseHashGrid(seed);
            //HexUnit.unitPrefab = unitPrefab; //temp solution
        }
    }

    private void CreateCells()
    {
        cells = new HexCell[cellCountZ * cellCountX];

        for (int z = 0, i = 0; z < cellCountZ; z++)
        {
            for (int x = 0; x < cellCountX; x++)
            {
                CreateCell(x, z, i++);
            }
        }
    }

    public bool CreateMap(int x, int z)
    {
        if(x<= 0 || x % HexMetrics.chunkSizeX != 0 || z<=0 || z % HexMetrics.chunkSizeZ != 0)
        {
            Debug.LogError("Unsupported map size, the size needs to be divisable by the chunk size for correct chunking.");
            return false;
        }

        ClearPath();

        if(chunks != null)
        {
            for(int i = 0; i< chunks.Length; i++)
            {
                Destroy(chunks[i].gameObject);
            }
        }
        cellCountX = x;
        cellCountZ = z;

        chunkCountX  = cellCountX / HexMetrics.chunkSizeX;
        chunkCountZ  = cellCountZ / HexMetrics.chunkSizeZ;

        CreateChunks();
        CreateCells();

        return true;
    }

    void Awake()
    {
        HexMetrics.noiseSource = noiseSource; // cannot be assigned via editor so we are doing it here
        HexMetrics.InitialiseHashGrid(seed);
    }

    private void CreateChunks()
    {
        chunks = new HexGridChunk[chunkCountX * chunkCountZ];

        for (int z = 0, i = 0; z< chunkCountZ; z++)
        {
            for (int x = 0; x < chunkCountX; x++)
            {
                HexGridChunk chunk = chunks[i++] = Instantiate(chunkPrefab);
                chunk.transform.SetParent(transform);
            }
        }
    }

    //As the default planes are 10 by 10 units, offset each cell by that amount.
    //We know that the distance between adjacent hexagon cells in the X direction is equal to twice the inner radius. 
    //So let's use that. Also, the distance to the next row of cells should be 1.5 times the outer radius.

    //consecutive hexagon rows are not directly above each other.
    //Each row is offset along the X axis by the inner radius. 
    //We can do this by adding half of Z to X before multiplying by double the inner radius.
    void CreateCell(int x, int z, int i)
    {
        float columnOffset = z * 0.5f - z / 2;

        Vector3 position;
        position.x = (x + columnOffset) * (HexMetrics.innerRadius * 2f);
        position.y = 0f;
        position.z = z * (HexMetrics.outerRadius * 1.5f);

        HexCell cell = cells[i] = Instantiate<HexCell>(cellPrefab);
        cell.transform.localPosition = position;
        cell.coordinates = HexCoordinates.FromOffsetCoords(x, z);

        if (x > 0)
        {
            cell.SetNeighbour(HexDirection.W, cells[i - 1]);//set neighbour of previous cell created
        }
        if (z > 0)
        {
            if ((z & 1) == 0)//checks for even numbers. Even binary numbers always have 0 as least significant bit
            {
                cell.SetNeighbour(HexDirection.SE, cells[i - cellCountX]);
                if (x > 0)//ignore first cell in each row as it has no SW neighbour
                {
                    cell.SetNeighbour(HexDirection.SW, cells[i - cellCountX - 1]);
                }
            }
            else //odd numbers now
            {
                cell.SetNeighbour(HexDirection.SW, cells[i - cellCountX]);
                if (x < cellCountX - 1) //ignore cells on the far east
                {
                    cell.SetNeighbour(HexDirection.SE, cells[i - cellCountX + 1]);
                }
            }

        }

        Text label = Instantiate<Text>(cellLabelPrefab);
        label.rectTransform.anchoredPosition = new Vector2(position.x, position.z);

        cell.uiRect = label.rectTransform;

        cell.Elevation = 0;

        cell.DisableHighlight(); //I seem to be having trouble enabling them if I disable them in editor
       // cell.SetLabel(cell.coordinates.ToString());

        AddCellToChunk(x, z, cell);
    }

    private void AddCellToChunk(int x, int z, HexCell cell)
    {
        int chunkX = x / HexMetrics.chunkSizeX;
        int chunkZ = z / HexMetrics.chunkSizeZ;
        HexGridChunk chunk = chunks[chunkX + chunkZ * chunkCountX];

        int localX = x - chunkX * HexMetrics.chunkSizeX;
        int localZ = z - chunkZ * HexMetrics.chunkSizeZ;
        chunk.AddCell(localX + localZ * HexMetrics.chunkSizeX, cell);

    }

    public HexCell GetCell(Vector3 position)
    {
        position = transform.InverseTransformPoint(position);
        HexCoordinates coordinates = HexCoordinates.FromPosition(position);
        int index = coordinates.X + coordinates.Z * cellCountX + coordinates.Z / 2;

        return cells[index];
    }

    public HexCell GetCell(HexCoordinates coordinates)
    {
        int z = coordinates.Z;
        if(z <0 || z>= cellCountZ)
        {
            return null;
        }

        int x = coordinates.X + z/2;
        if (x < 0 || x >= cellCountX)
        {
            return null;
        }
        return cells[x + z * cellCountX];
    }

    public void ShowUI(bool visible)
    {
        for (int i = 0; i< chunks.Length; i++)
        {
            chunks[i].ShowUI(visible);
        }
    }

    public void Save(BinaryWriter writer)
    {
        writer.Write(cellCountX);
        writer.Write(cellCountZ);

        for (int i = 0; i < cells.Length; i++)
        {
            cells[i].Save(writer);
        }

        writer.Write(GameMode.units.Count);
        for (int i = 0; i < GameMode.units.Count; i++)
        {
            GameMode.units[i].Save(writer);
        }
    }

    public void Load(BinaryReader reader, int header)
    {
        ClearPath();
        GameMode.ClearUnits();

        int x = 20, z = 15;

        if (header>=1)
        {
            x = reader.ReadInt32();
            z = reader.ReadInt32();
        }

        if(x != cellCountX || z != cellCountZ)
        {
            if(!CreateMap(x, z))
            {
                return;
            }
        }

        for (int i = 0; i < cells.Length; i++)
        {
            cells[i].Load(reader, header);
        }

        for (int i = 0; i < chunks.Length; i++)
        {
            chunks[i].Refresh();
        }

        //if (header >= 2)
        //{
        //    int unitCount = reader.ReadInt32();
        //    for (int i = 0; i < unitCount; i++)
        //    {
        //        HexUnit.Load(reader, this, header);
        //    }
        //}
    }

    public void FindPathBetween(HexCell fromCell, HexCell toCell, HexUnit unit)
    {
        ClearPath();
        currentPathFrom = fromCell;
        currentPathTo = toCell;
        
        currentPathExists = Search(fromCell, toCell, unit);
        ShowPath(unit.MoveBudget);
    }
    
    void ShowPath(int moveBudget)
    {
        if(currentPathExists)
        {
            HexCell current = currentPathTo;
            while (current != currentPathFrom)
            {
                //int turnsToTravel = (current.DistanceToCell - 1) / moveBudget; //uses integer division. 1 more means that you can't travel the distance in this turn
                //                                                                    //adjusted by -1 
                //current.SetLabel(turnsToTravel.ToString());
                current.EnableHighlight(Color.white);
                current = current.PathFrom;
            }
            //currentPathFrom.EnableHighlight(Color.blue);
            currentPathTo.EnableHighlight(Color.yellow);
        }
        else
        {
            //currentPathFrom.EnableHighlight(Color.cyan);
            currentPathTo.EnableHighlight(Color.red);
        }
    }

    public void ClearPath()
    {
        if(currentPathExists)
        {
            HexCell current = currentPathTo;
            while (current != currentPathFrom)
            {
                current.SetLabel(null);
                current.DisableHighlight();
                current = current.PathFrom;
            }
            current.DisableHighlight();
            currentPathExists = false;
        }
        currentPathFrom = currentPathTo = null;
    }

    bool Search(HexCell fromCell, HexCell toCell, HexUnit unit)
    {
        int moveBudget = unit.MoveBudget;

        searchFrontierPhase += 2;

        if(searchFrontier == null)
        {
            searchFrontier = new HexCellPriorityQueue();
        }
        else
        {
            searchFrontier.Clear();
        }

        fromCell.SearchPhase = searchFrontierPhase;
        fromCell.DistanceToCell = 0;
        searchFrontier.Enqueue(fromCell);

        while(searchFrontier.Count > 0)
        {
            HexCell current = searchFrontier.Dequeue(); //take out of frontier
            current.SearchPhase += 1;

            if (current == toCell)
            {
                return true;
            }

           // int turnsToTravel = (current.DistanceToCell - 1) / moveBudget; //uses integer division. 1 or more means that you can't travel the distance in this turn
                                                                           //uses distance -1 because we are using integer division, this means that remainders are chopped off

            int turnsToTravel = unit.GetTurnsToTravel(current.DistanceToCell - 1);
            if(turnsToTravel > 0)
            {
                return false;
            }

            for (HexDirection d = HexDirection.NE; d<= HexDirection.NW; d++)
            {
                HexCell neighbour = current.GetNeighbour(d);

                if(neighbour == null || neighbour.SearchPhase > searchFrontierPhase)// /*|| neighbour.DistanceToSelectedCell != int.MaxValue*/ || neighbour.IsUnderwater || neighbour.Unit) //ignore cells on certain conditions
                {
                    continue;
                }

                if(!unit.IsValidDestination(neighbour))
                {
                    continue;
                }

                int moveCost = unit.GetMoveCost(current, neighbour, d);
                if(moveCost < 0)
                {
                    continue;
                }

                int distance = current.DistanceToCell + moveCost;
                int neighbourTurnsToTravel = (distance - 1) / moveBudget;

                if(neighbourTurnsToTravel > turnsToTravel)
                {
                    distance = neighbourTurnsToTravel * moveBudget + moveCost;
                }

                if(neighbour.SearchPhase < searchFrontierPhase)
                {
                    neighbour.DistanceToCell = distance;
                    neighbour.SearchPhase = searchFrontierPhase;
                    neighbour.PathFrom = current;
                    neighbour.SearchHeuristic = neighbour.coordinates.DistanceTo(toCell.coordinates);
                    searchFrontier.Enqueue(neighbour);
                }
                else if(distance < neighbour.DistanceToCell)
                {
                    int oldPriority = neighbour.SearchPriority;
                    neighbour.DistanceToCell = distance;
                    neighbour.PathFrom = current;
                    searchFrontier.Change(neighbour, oldPriority);
                }
            }
        }

        return false;
    }

    public void DeployUnits()
    {
        foreach (var unit in GameMode.units)
        {
            SetUnitLocation(unit);//, tempLocation);
        }
    }

    void SetUnitLocation(HexUnit unit)//, HexCell location)
    {
        HexCell location = GetCell(UnityEngine.Random.Range(0, cells.Length));

        if (unit.IsValidDestination(location))
        {
            unit.Location = location;
        }
        else
        {
            foreach (var neighbour in location.Neighbours)
            {
                if(unit.IsValidDestination(neighbour))
                {
                    Debug.Log("Choosing neighbour");
                    unit.Location = neighbour;
                    return;
                }
            }
            SetUnitLocation(unit);
        }
    }

    public void AddUnit(HexUnit unit, /*float orientation,*/ int player, HashSet<DamageType> vulnerabilties, HashSet<DamageType> resistances)
    {
        GameMode.units.Add(unit);
        unit.transform.SetParent(transform, false);
        unit.Player = player;

        unit.Ability = unit.GetComponentInChildren<UnitPower>();// as UnitPower; 
        if (unit.Ability == null)
        {
            Debug.Log("Get component not working as intended");
        }
        unit.Resistances = resistances;
        unit.Vulnerabilities = vulnerabilties;
    }

    public void AddUnit(HexUnit unit, HexCell location, float orientation, int player, /*UnitPower power,*/ HashSet<DamageType> vulnerabilties, HashSet<DamageType> resistances)
    {
        GameMode.units.Add(unit);
        unit.transform.SetParent(transform, false);
        unit.Location = location;
        unit.Orientation = orientation;
        unit.Player = player;

        // power.InitDefault();
        //unit.Ability = power;
        unit.Ability = unit.GetComponentInChildren<UnitPower>();// as UnitPower; 
        if(unit.Ability == null)
        {
            Debug.Log("Get component not working as intended");
        }
        unit.Resistances = resistances;
        unit.Vulnerabilities = vulnerabilties;
        unit.Health = 5f;
    }

    public void RemoveUnit(HexUnit unit)
    {
        GameMode.units.Remove(unit);
        unit.Die();
    }

    public HexCell GetCell()//returns cell under pointer 
    {
        return GetCell(Camera.main.ScreenPointToRay(Input.mousePosition));
    }

    public HexCell GetCell(Ray ray)
    {
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            return GetCell(hit.point);
        }

        return null;
    }

    public List<HexCell> GetPath()
    {
        if(!currentPathExists)
        {
            return null;
        }

        List<HexCell> path = ListPool<HexCell>.Get();

        for(HexCell c = currentPathTo; c != currentPathFrom; c = c.PathFrom)
        {
            path.Add(c);
        }

        path.Add(currentPathFrom); //need to manually add the starting path

        path.Reverse();//reverse the path so that it starts at the units location

        return path;
    }

    public HexCell GetCell(int xOffset, int zOffset)
    {
        return cells[xOffset + zOffset * cellCountX];
    }

    public HexCell GetCell(int cellIndex)
    {
        return cells[cellIndex];
    }
}
