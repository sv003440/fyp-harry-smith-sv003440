﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewMapMenu : UIBase
{
    public HexGrid hexGrid;
    public HexMapGenerator mapGenerator;

    bool generateMaps = true;

    void CreateMap(int x, int z)
    {
        if (generateMaps)
        {
            mapGenerator.GenerateMap(x, z);
        }
        else
        {
            hexGrid.CreateMap(x, z);
        }
        HexMapCamera.ValidatePosition();

        Close();

    }

    public void CreateSmallMap()
    {
        CreateMap(20, 15);
    }

    public void CreateMediumMap()
    {
        CreateMap(40, 30);
    }

    public void CreateLargeMap()
    {
        CreateMap(80, 60);
    }

    public void ToggleMapGenerator(bool toggle)
    {
        generateMaps = toggle;
    }
}
