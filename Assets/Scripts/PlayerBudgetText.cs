﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBudgetText : MonoBehaviour {

    public Text budgetTrackerText;
    // Use this for initialization
    void Start()
    {
        UpdateText();
    }

    private void OnEnable()
    {
        UpdateText();
    }

    public void UpdateText()
    {
        budgetTrackerText.text = "Player " + GameMode.currentPlayer + " has " + GameMode.CurrentTurnBudget + " budget left";
    }

    void Update()
    {
        if(Input.GetButtonDown("UseAbility"))
        {
            UpdateText();
        }
    }
}
