﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBase : MonoBehaviour {

    static string previousScreen;

    GameObject previous;

    public void Open(GameObject cameFrom = null)
    {
        previous = cameFrom;

        if (cameFrom)
        {
            previous.GetComponent<UIBase>().Close();
        }

        gameObject.SetActive(true);
        HexMapCamera.Locked = true;
    }

    public void Close()
    {
       // previous = gameObject;

        gameObject.SetActive(false);
        HexMapCamera.Locked = false;
    }

    public void GoBack()
    {
        if(previous)
        {
            previous.GetComponent<UIBase>().Open(gameObject);
        }
        else
        {
            Close();
        }

        //Close();
    }
}
