﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class SaveLoadMenu : UIBase {

    public HexGrid hexGrid;

    bool saveMode;

    public Text menuLable, actionButtonLabel;
    public InputField nameInput;

    public RectTransform listContent;

    public SaveLoadItem itemPrefab;

    const int versionNumber = 4;

    public void Open(bool saveMode)
    {
        this.saveMode = saveMode;

        if(saveMode)
        {
            menuLable.text = "Save Map";
            actionButtonLabel.text = "Save";
        }
        else
        {
            menuLable.text = "Load Map";
            actionButtonLabel.text = "Load";
        }

        FillList();

        Open();
        //gameObject.SetActive(true);
        //HexMapCamera.Locked = true;
    }

    string GetSelectedPath()
    {
        string mapName = nameInput.text;

        if(mapName.Length == 0)
        {
            return null;
        }

        return Path.Combine(Application.persistentDataPath, mapName + ".map"); //content type of input field controlled in editor so people shouldn't be able to save and load from uncontrolled locations with a path seperator
    }

    public void Action()
    {
        string path = GetSelectedPath();
        if(path == null)
        {
            return;
        }
        if(saveMode)
        {
            Save(path);
        }
        else
        {
            Load(path);
        }

        Close();
    }

    public void Save(string path)
    {
        //string path = Path.Combine(Application.persistentDataPath, "test.map");
        using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create)))
        {
            writer.Write(versionNumber);//four bytes to be used as a header allowing us to check these bytes first to see what save format to load
            hexGrid.Save(writer);
            GameMode.Save(writer);
        } //automatically closes when scope is exited - this will also take care of early return, exceptions and errors

    }

    public void Load(string path)
    {
        //string path = Path.Combine(Application.persistentDataPath, "test.map");

        if(!File.Exists(path))
        {
            Debug.LogError("File does not exist, check path: " + path);
            return;
        }
        using (BinaryReader reader = new BinaryReader(File.OpenRead(path)))
        {
            int header = reader.ReadInt32();
            if (header <= versionNumber)
            {
                hexGrid.Load(reader, header);
                HexMapCamera.ValidatePosition();
                GameMode.Load(reader, header);
            }
            else
            {
                Debug.LogWarning("Unknown map format, check header: " + header);
            }
        }
    }

    public void SelectItem(string name)
    {
        nameInput.text = name;
    }

    void FillList()
    {
        for (int i = 0; i < listContent.childCount; i++)
        {
            Destroy(listContent.GetChild(i).gameObject);
        }

        string[] paths = Directory.GetFiles(Application.persistentDataPath, "*.map");
        Array.Sort(paths);

        for(int i = 0; i < paths.Length; i++)
        {
            SaveLoadItem item = Instantiate(itemPrefab);
            item.menu = this;
            item.MapName = Path.GetFileNameWithoutExtension(paths[i]);
            item.transform.SetParent(listContent, false);
        }
    }

    public void Delete()
    {
        string path = GetSelectedPath();
        if(path == null)
        {
            return;
        }
        if(File.Exists(path))
        {
            File.Delete(path);
        }
        nameInput.text = "";
        FillList();
    }


}
