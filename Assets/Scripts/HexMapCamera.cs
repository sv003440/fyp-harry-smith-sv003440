﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexMapCamera : MonoBehaviour
{
    public HexGrid grid; //added in with inspector
    Transform swivel;
    public Transform stick;

    float zoom = 1f;

    public float stickMinZoom = -250f;
    public float stickMaxZoom = 50f;

    public float swivelMinZoom = 90f;
    public float swivelMaxZoom = 45f;
    public float moveSpeedMinZoom = 400f;
    public float moveSpeedMaxZoom = 100f;
    public float rotationSpeed = 180f;
    float rotationAngle;

    public static HexMapCamera instance;

    public static bool Locked
    {
        set
        {
            instance.enabled = !value;
        }
    }

    private void Start()
    {
        instance.enabled = false;
    }

    void Awake()
    {
        instance = this;
        swivel = transform.GetChild(0);
        stick = swivel.GetChild(0);
    }

    public void ControlCamera()
    {
        float zoomDelta = Input.GetAxis("Mouse ScrollWheel");
        if (zoomDelta != 0f)
        {
            AdjustZoom(zoomDelta);
        }

        float rotationDelta = Input.GetAxis("Rotation");
        if (rotationDelta != 0)
        {
            AdjustRotation(rotationDelta);
        }

        float xDelta = Input.GetAxis("Horizontal");
        float zDelta = Input.GetAxis("Vertical");
        if (xDelta != 0 || zDelta != 0)
        {
            AdjustPosition(xDelta, zDelta);
        }
    }

    // Update is called once per frame
    void Update()
    {
        ControlCamera();
    }

    private void AdjustRotation(float delta)
    {
        rotationAngle += delta * rotationSpeed * Time.deltaTime;

        if(rotationAngle < 0f)
        {
            rotationAngle += 360f;
        }
        else if(rotationAngle >= 360f)
        {
            rotationAngle -= 360f;
        }
        transform.localRotation = Quaternion.Euler(0f, rotationAngle, 0f);
    }

    private void AdjustPosition(float xDelta, float zDelta)
    {
        Vector3 direction = transform.localRotation * new Vector3(xDelta, 0f, zDelta).normalized;
        float damping = Mathf.Max(Mathf.Abs(xDelta), Mathf.Abs(zDelta)); //stops drift after releasing keys
        float distance = Mathf.Lerp(moveSpeedMinZoom, moveSpeedMaxZoom, zoom) * Time.deltaTime * damping;

        Vector3 position = transform.localPosition;
        position +=  direction * distance;
        transform.localPosition = ClampPosition(position);
    }

    private Vector3 ClampPosition(Vector3 position)
    {
        float xMax = (grid.cellCountX - 0.5f) * (2f * HexMetrics.innerRadius);
        position.x = Mathf.Clamp(position.x, 0f, xMax);

        float zMax = (grid.cellCountZ - 1f) * (/*0.5f * */HexMetrics.outerRadius);
        position.z = Mathf.Clamp(position.z, -zMax, zMax);

        return position;
    }

    private void AdjustZoom(float delta)
    {
        zoom = Mathf.Clamp01(zoom + delta);

        float distance = Mathf.Lerp(stickMinZoom, stickMaxZoom, zoom);
        stick.localPosition = new Vector3(0f, 0f, distance);

        float angle = Mathf.Lerp(swivelMinZoom, swivelMaxZoom, zoom);
        swivel.localRotation = Quaternion.Euler(angle, 0f, 0f);
    }

    public static void ValidatePosition()
    {
        instance.AdjustPosition(0f, 0f);
    }
}
