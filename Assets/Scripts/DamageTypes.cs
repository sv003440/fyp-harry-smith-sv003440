﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageType
{
    None,
    Fire,
    Water,
    Air,
    Earth,
    Force,
    Healing
}
