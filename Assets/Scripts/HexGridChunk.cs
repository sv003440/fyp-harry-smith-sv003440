﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HexGridChunk : MonoBehaviour
{
    HexCell[] cells;

    public HexMesh terrain, rivers, roads, water, waterShore, estuaries;
    Canvas gridCanvas;

    public HexFeatureManager featureManager;

    static Color colour1 = new Color(1f, 0f, 0f);
    static Color colour2 = new Color(0f, 1f, 0f);
    static Color colour3 = new Color(0f, 0f, 1f);

    private void Awake()
    {
        gridCanvas = GetComponentInChildren<Canvas>();

        cells = new HexCell[HexMetrics.chunkSizeX * HexMetrics.chunkSizeZ];
    }

    void LateUpdate() //to ensure triangulation happens after all editing. 
    {
        Triangulate();
        enabled = false;
    }

    internal void AddCell(int index, HexCell cell)
    {
        cells[index] = cell;
        cell.chunk = this;
        cell.transform.SetParent(transform, false);
        cell.uiRect.SetParent(gridCanvas.transform, false);
    }

    public void Refresh()
    {
        //hexMesh.Triangulate(cells);
        enabled = true;
    }

    public void ShowUI(bool visible)
    {
        gridCanvas.gameObject.SetActive(visible);
    }

    public void Triangulate()
    {
        terrain.Clear();
        rivers.Clear();
        roads.Clear();
        water.Clear();
        waterShore.Clear();
        estuaries.Clear();
        featureManager.Clear();

        for (int i = 0; i < cells.Length; i++)
        {
            Triangulate(cells[i]);
        }

        terrain.Apply();
        rivers.Apply();
        roads.Apply();
        water.Apply();
        waterShore.Apply();
        estuaries.Apply();
        featureManager.Apply();
    }

    void Triangulate(HexCell cell)
    {
        for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
        {
            Triangulate(d, cell);
        }
        if (!cell.IsUnderwater && !cell.HasRiver && !cell.HasRoads)
        {
            featureManager.AddFeature(cell, cell.Position);
        }
    }

    void Triangulate(HexDirection direction, HexCell cell)
    {
        Vector3 centre = cell.Position;
        EdgeVertices e = new EdgeVertices(centre + HexMetrics.GetFirstSolidCorner(direction), centre + HexMetrics.GetSecondSolidCorner(direction));

        if (cell.HasRiver)
        {
            if (cell.HasRiverThroughEdge(direction))
            {
                e.v3.y = cell.StreamBedY;
                if (cell.HasRiverBeginOrEnd)
                {
                    TriangulateWithRiverBeginOrEnd(direction, cell, centre, e);
                }
                else
                {
                    TriangulateWithRiver(direction, cell, centre, e);
                }
            }
            else
            {
                TriangulateAdjacentToRiver(direction, cell, centre, e);
            }
        }
        else
        {
            TriangulateWithoutRiver(direction, cell, centre, e);

            if(!cell.IsUnderwater && !cell.HasRoadThroughEdge(direction))
            {
                featureManager.AddFeature(cell, (centre + e.v1 + e.v5) * (1f / 3f));
            }
        }

        if (direction <= HexDirection.SE) //only triangulate in NE, E, SE directions needed
        {
            TriangulateConnection(direction, cell, e);
        }

        if(cell.IsUnderwater)
        {
            TriangulateWater(direction, cell, centre);
        }
    }

    private void TriangulateWater(HexDirection direction, HexCell cell, Vector3 centre)
    {
        centre.y = cell.WaterSurfaceY;

        HexCell neighbour = cell.GetNeighbour(direction);

        if(neighbour != null && !neighbour.IsUnderwater)
        {
            TriangulateWaterShore(direction, cell, neighbour, centre);
        }
        else
        {
            TriangulateOpenWater(direction, cell, neighbour, centre);
        }
    }

    private void TriangulateOpenWater(HexDirection direction, HexCell cell, HexCell neighbour, Vector3 centre)
    {
        Vector3 c1 = centre + HexMetrics.GetFirstWaterCorner(direction);
        Vector3 c2 = centre + HexMetrics.GetSecondWaterCorner(direction);

        water.AddTriangle(centre, c1, c2);

        if (direction <= HexDirection.SE && neighbour != null)
        {
            Vector3 bridge = HexMetrics.GetWaterBridge(direction);
            Vector3 e1 = c1 + bridge;
            Vector3 e2 = c2 + bridge;

            water.AddQuad(c1, c2, e1, e2);

            if (direction <= HexDirection.E)
            {
                HexCell nextNeighbour = cell.GetNeighbour(direction.Next());
                if (nextNeighbour == null || !nextNeighbour.IsUnderwater)
                {
                    return;
                }
                water.AddTriangle(c2, e2, c2 + HexMetrics.GetWaterBridge(direction.Next()));
            }
        }
    }

    private void TriangulateWaterShore(HexDirection direction, HexCell cell, HexCell neighbour, Vector3 centre)
    {
        EdgeVertices e1 = new EdgeVertices(centre + HexMetrics.GetFirstWaterCorner(direction), centre + HexMetrics.GetSecondWaterCorner(direction));

        water.AddTriangle(centre, e1.v1, e1.v2);
        water.AddTriangle(centre, e1.v2, e1.v3);
        water.AddTriangle(centre, e1.v3, e1.v4);
        water.AddTriangle(centre, e1.v4, e1.v5);

        Vector3 centre2 = neighbour.Position;
        centre2.y = centre.y;

        EdgeVertices e2 = new EdgeVertices(centre2 + HexMetrics.GetSecondSolidCorner(direction.Opposite()), centre2 + HexMetrics.GetFirstSolidCorner(direction.Opposite()));

        if (cell.HasRiverThroughEdge(direction))
        {
            TriangulateEstuary(e1, e2, cell.IncomingRiver == direction);
        }
        else
        {
            waterShore.AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
            waterShore.AddQuad(e1.v2, e1.v3, e2.v2, e2.v3);
            waterShore.AddQuad(e1.v3, e1.v4, e2.v3, e2.v4);
            waterShore.AddQuad(e1.v4, e1.v5, e2.v4, e2.v5);
            waterShore.AddQuadUV(0f, 0f, 0f, 1f);
            waterShore.AddQuadUV(0f, 0f, 0f, 1f);
            waterShore.AddQuadUV(0f, 0f, 0f, 1f);
            waterShore.AddQuadUV(0f, 0f, 0f, 1f);
        }

        HexCell nextNeighbour = cell.GetNeighbour(direction.Next());
        if(nextNeighbour != null)
        {
            Vector3 v3 = nextNeighbour.Position + (nextNeighbour.IsUnderwater ? HexMetrics.GetFirstWaterCorner(direction.Previous()) : HexMetrics.GetFirstSolidCorner(direction.Previous()));
            v3.y = centre.y;

            waterShore.AddTriangle(e1.v5, e2.v5, v3);
            waterShore.AddTriangleUV(new Vector2(0f, 0f), new Vector2(0f, 1f), new Vector2(0f, nextNeighbour.IsUnderwater ? 0f : 1f));
        }
    }

    private void TriangulateEstuary(EdgeVertices e1, EdgeVertices e2, bool incomingRiver)
    {
        waterShore.AddTriangle(e2.v1, e1.v2, e1.v1);
        waterShore.AddTriangle(e2.v5, e1.v5, e1.v4);
        waterShore.AddTriangleUV(new Vector2(0f, 1f), new Vector2(0f, 0f), new Vector2(0f, 0f));
        waterShore.AddTriangleUV(new Vector2(0f, 1f), new Vector2(0f, 0f), new Vector2(0f, 0f));

        estuaries.AddQuad(e2.v1, e1.v2, e2.v2, e1.v3);
        estuaries.AddTriangle(e1.v3, e2.v2, e2.v4);
        estuaries.AddQuad(e1.v3, e1.v4, e2.v4, e2.v5);

        estuaries.AddQuadUV(new Vector2(0f, 1f), new Vector2(0f, 0f), new Vector2(1f, 1f), new Vector2(0f, 0f));
        estuaries.AddTriangleUV(new Vector2(0f, 0f), new Vector2(1f, 1f), new Vector2(1f, 1f));
        estuaries.AddQuadUV(new Vector2(0f, 0f), new Vector2(0f, 0f), new Vector2(1f, 1f), new Vector2(0f, 1f));

        if (incomingRiver)
        {
            estuaries.AddQuadUV2(new Vector2(1.5f, 1f), new Vector2(0.7f, 1.15f), new Vector2(1f, 0.8f), new Vector2(0.5f, 1.1f));
            estuaries.AddTriangleUV2(new Vector2(0.5f, 1.1f), new Vector2(1f, 0.8f), new Vector2(0f, 0.8f));
            estuaries.AddQuadUV2(new Vector2(0.5f, 1.1f), new Vector2(0.3f, 1.15f), new Vector2(0f, 0.8f), new Vector2(-0.5f, 1f));
        }
        else
        {
            estuaries.AddQuadUV2(new Vector2(-0.5f, -0.2f), new Vector2(0.3f, -0.35f), new Vector2(0f, 0f), new Vector2(0.5f, -0.3f));
            estuaries.AddTriangleUV2(new Vector2(0.5f, -0.3f), new Vector2(0f, 0f), new Vector2(1f, 0f));
            estuaries.AddQuadUV2(new Vector2(0.5f, -0.3f), new Vector2(0.7f, -0.35f), new Vector2(1f, 0f), new Vector2(1.5f, 0.2f));
        }

    }

    private void TriangulateWithoutRiver(HexDirection direction, HexCell cell, Vector3 centre, EdgeVertices e)
    {
        TriangulateEdgeFan(centre, e, cell.TerrainTypeIndex);

        if (cell.HasRoads)
        {
            Vector2 interpolators = GetRoadInterpolators(direction, cell);
            TriangulateRoad(centre, Vector3.Lerp(centre, e.v1, interpolators.x), Vector3.Lerp(centre, e.v5, interpolators.y), e, cell.HasRoadThroughEdge(direction));
        }
    }

    private void TriangulateAdjacentToRiver(HexDirection direction, HexCell cell, Vector3 centre, EdgeVertices e)
    {
        if(cell.HasRoads)
        {
            TriangulateRoadAdjacentToRiver(direction, cell, centre, e);
        }
        if (cell.HasRiverThroughEdge(direction.Next()))
        {
            if (cell.HasRiverThroughEdge(direction.Previous()))
            {
                centre += HexMetrics.GetSolidEdgeMiddle(direction) * (HexMetrics.InnerToOuter * 0.5f);
            }
            else if (cell.HasRiverThroughEdge(direction.Previous2()))
            {
                centre += HexMetrics.GetFirstSolidCorner(direction) * 0.25f;
            }
        }
        else if (cell.HasRiverThroughEdge(direction.Previous()) && cell.HasRiverThroughEdge(direction.Next2()))
        {
            centre += HexMetrics.GetSecondSolidCorner(direction) * 0.25f;
        }
        EdgeVertices m = new EdgeVertices(Vector3.Lerp(centre, e.v1, 0.5f), Vector3.Lerp(centre, e.v5, 0.5f));

        TriangulateEdgeStrip(m, colour1, cell.TerrainTypeIndex, e, colour1, cell.TerrainTypeIndex);
        TriangulateEdgeFan(centre, m, cell.TerrainTypeIndex);

        if(!cell.IsUnderwater && !cell.HasRoadThroughEdge(direction))
        {
            featureManager.AddFeature(cell, (centre + e.v1 + e.v5) * (1f / 3f));
        }
    }

    private void TriangulateRoadAdjacentToRiver(HexDirection direction, HexCell cell, Vector3 centre, EdgeVertices e)
    {
        bool hasRoadThroughEdge = cell.HasRoadThroughEdge(direction);
        bool previousHasRiver = cell.HasRiverThroughEdge(direction.Previous());
        bool nextHasRiver = cell.HasRiverThroughEdge(direction.Next());

        Vector2 interpolators = GetRoadInterpolators(direction, cell);
        Vector3 roadCentre = centre;

        if(cell.HasRiverBeginOrEnd)
        {
            roadCentre += HexMetrics.GetSolidEdgeMiddle(cell.RiverBeginOrEndDirection.Opposite()) * (1f / 3f);
        }
        else if(cell.IncomingRiver == cell.OutgoingRiver.Opposite())
        {
            Vector3 corner;
            if( previousHasRiver)
            {
                if(!hasRoadThroughEdge && !cell.HasRoadThroughEdge(direction.Next()))
                {
                    return;
                }
                corner = HexMetrics.GetSecondSolidCorner(direction);
            }
            else
            {
                if(!hasRoadThroughEdge && !cell.HasRoadThroughEdge(direction.Previous()))
                {
                    return;
                }
                corner = HexMetrics.GetFirstSolidCorner(direction);
            }
            roadCentre += corner * 0.5f;
            if(cell.IncomingRiver == direction.Next() // avoids generating extra bridges in the same place
                && (cell.HasRoadThroughEdge(direction.Next2()) || cell.HasRoadThroughEdge(direction.Opposite()))  ) //check if there is a road on other side of river to connect the bridge to
            {
                featureManager.AddBridge(roadCentre, centre - corner * 0.5f);
            }

            centre += corner * 0.25f;
        }
        else if(cell.IncomingRiver == cell.OutgoingRiver.Previous())
        {
            roadCentre -= HexMetrics.GetSecondCorner(cell.IncomingRiver) * 0.2f;
        }
        else if(cell.IncomingRiver == cell.OutgoingRiver.Next())
        {
            roadCentre -= HexMetrics.GetFirstCorner(cell.IncomingRiver) * 0.2f;
        }
        else if(previousHasRiver && nextHasRiver)
        {
            if(!hasRoadThroughEdge)
            {
                return;
            }
            Vector3 offset = HexMetrics.GetSolidEdgeMiddle(direction) * HexMetrics.InnerToOuter;
            roadCentre += offset * 0.7f;
            centre += offset * 0.5f;
        }
        else
        {
            HexDirection middle;
            if(previousHasRiver)
            {
                middle = direction.Next();
            }
            else if(nextHasRiver)
            {
                middle = direction.Previous();
            }
            else
            {
                middle = direction;
            }

            if (!cell.HasRoadThroughEdge(middle) && !cell.HasRoadThroughEdge(middle.Previous()) && !cell.HasRoadThroughEdge(middle.Next()))
            {
                return;
            }
 
            Vector3 offset = HexMetrics.GetSolidEdgeMiddle(middle);
            roadCentre += offset * 0.25f;

            if (direction == middle //avoiding multiple bridges in the same place
                && cell.HasRoadThroughEdge(direction.Opposite()) )
            {
                featureManager.AddBridge(roadCentre, centre - offset * (HexMetrics.InnerToOuter * 0.7f));
            }
        }

        Vector3 mL = Vector3.Lerp(roadCentre, e.v1, interpolators.x);
        Vector3 mR = Vector3.Lerp(roadCentre, e.v5, interpolators.y);

        TriangulateRoad(roadCentre, mL, mR, e, hasRoadThroughEdge);

        if(previousHasRiver)
        {
            TriangulateRoadEdge(roadCentre, centre, mL);
        }
        if (nextHasRiver)
        {
            TriangulateRoadEdge(roadCentre, mR, centre);
        }
    }

    private void TriangulateWithRiverBeginOrEnd(HexDirection direction, HexCell cell, Vector3 centre, EdgeVertices e)
    {
        EdgeVertices m = new EdgeVertices(Vector3.Lerp(centre, e.v1, 0.5f), Vector3.Lerp(centre, e.v5, 0.5f));

        m.v3.y = e.v3.y;

        TriangulateEdgeStrip(m, colour1, cell.TerrainTypeIndex, e, colour1, cell.TerrainTypeIndex);
        TriangulateEdgeFan(centre, m, cell.TerrainTypeIndex);

        if (!cell.IsUnderwater)
        {
            bool reversed = cell.HasIncomingRiver;
            TriangulateRiverQuad(m.v2, m.v4, e.v2, e.v4, cell.RiverSurfaceY, 0.6f, reversed);

            centre.y = m.v2.y = m.v4.y = cell.RiverSurfaceY;
            rivers.AddTriangle(centre, m.v2, m.v4);
            if (reversed)
            {
                rivers.AddTriangleUV(new Vector2(0.5f, 0.4f), new Vector2(1f, 0.2f), new Vector2(0f, 0.2f));
            }
            else
            {
                rivers.AddTriangleUV(new Vector2(0.5f, 0.4f), new Vector2(0f, 0.6f), new Vector2(1f, 0.6f));
            }
        }
    }

    private void TriangulateWithRiver(HexDirection direction, HexCell cell, Vector3 centre, EdgeVertices e) //creates a channel where previously there was a triangle
    {
        Vector3 centreL, centreR;
        if (cell.HasRiverThroughEdge(direction.Opposite()))
        {
            centreL = centre + HexMetrics.GetFirstSolidCorner(direction.Previous()) * 0.25f;
            centreR = centre + HexMetrics.GetSecondSolidCorner(direction.Next()) * 0.25f;
        }
        else if (cell.HasRiverThroughEdge(direction.Next()))
        {
            centreL = centre;
            centreR = Vector3.Lerp(centre, e.v5, 2f / 3f);
        }
        else if (cell.HasRiverThroughEdge(direction.Previous()))
        {
            centreL = Vector3.Lerp(centre, e.v1, 2f / 3f);
            centreR = centre;
        }
        else if (cell.HasRiverThroughEdge(direction.Next2()))
        {
            centreL = centre;
            centreR = centre + HexMetrics.GetSolidEdgeMiddle(direction.Next()) * (0.5f * HexMetrics.InnerToOuter);
        }
        else
        {
            centreL = centre + HexMetrics.GetSolidEdgeMiddle(direction.Previous()) * (0.5f * HexMetrics.InnerToOuter);

            centreR = centre;
        }
        centre = Vector3.Lerp(centreL, centreR, 0.5f);

        EdgeVertices m = new EdgeVertices(Vector3.Lerp(centreL, e.v1, 0.5f), Vector3.Lerp(centreR, e.v5, 0.5f), 1f / 6f);

        m.v3.y = centre.y = e.v3.y;

        TriangulateEdgeStrip(m, colour1, cell.TerrainTypeIndex, e, colour1, cell.TerrainTypeIndex);

        terrain.AddTriangle(centreL, m.v1, m.v2);
        terrain.AddQuad(centreL, centre, m.v2, m.v3);
        terrain.AddQuad(centre, centreR, m.v3, m.v4);
        terrain.AddTriangle(centreR, m.v4, m.v5);

        Vector3 types;
        types.x = types.y = types.z = cell.TerrainTypeIndex;
        terrain.AddTriangleTerrainTypes(types);
        terrain.AddQuadTerrainTypes(types);
        terrain.AddQuadTerrainTypes(types);
        terrain.AddTriangleTerrainTypes(types);

        terrain.AddTriangleColour(colour1);
        terrain.AddQuadColour(colour1);
        terrain.AddQuadColour(colour1);
        terrain.AddTriangleColour(colour1);

        if (!cell.IsUnderwater)
        {
            bool reversed = cell.IncomingRiver == direction;
            TriangulateRiverQuad(centreL, centreR, m.v2, m.v4, cell.RiverSurfaceY, 0.4f, reversed);
            TriangulateRiverQuad(m.v2, m.v4, e.v2, e.v4, cell.RiverSurfaceY, 0.6f, reversed);
        }
    }

    void TriangulateConnection(HexDirection direction, HexCell cell, EdgeVertices e1)
    {
        HexCell neighbour = cell.GetNeighbour(direction);

        if (neighbour == null)
        {
            return; //no need to connect if no neighbour present 
        }
        Vector3 bridge = HexMetrics.GetBridge(direction);
        bridge.y = neighbour.Position.y - cell.Position.y;

        EdgeVertices e2 = new EdgeVertices(e1.v1 + bridge, e1.v5 + bridge);

        bool hasRiverThroughEdge = cell.HasRiverThroughEdge(direction);
        bool hasRoadThroughEdge = cell.HasRoadThroughEdge(direction);

        if (hasRiverThroughEdge)
        {
            e2.v3.y = neighbour.StreamBedY;

            if (!cell.IsUnderwater)
            {
                if (!neighbour.IsUnderwater)
                {
                    TriangulateRiverQuad(e1.v2, e1.v4, e2.v2, e2.v4, cell.RiverSurfaceY, neighbour.RiverSurfaceY, 0.8f, cell.HasIncomingRiver && cell.IncomingRiver == direction);
                }
                else if (cell.Elevation > neighbour.WaterLevel)
                {
                    TriangulateWaterfallInWater(e1.v2, e1.v4, e2.v2, e2.v4, cell.RiverSurfaceY, neighbour.RiverSurfaceY, neighbour.WaterSurfaceY);
                }
            }
            else if (!neighbour.IsUnderwater && neighbour.Elevation > cell.WaterLevel)
            {
                TriangulateWaterfallInWater(e2.v4, e2.v2, e1.v4, e1.v2, neighbour.RiverSurfaceY, cell.RiverSurfaceY, cell.WaterSurfaceY);
            }
        }

        if (cell.GetEdgeType(direction) == HexEdgeType.Slope)
        {
            TriangulateEdgeTerraces(e1, cell, e2, neighbour, hasRoadThroughEdge);
        }
        else
        {
            TriangulateEdgeStrip(e1, colour1, cell.TerrainTypeIndex, e2, colour2, neighbour.TerrainTypeIndex, hasRoadThroughEdge);
        }

        featureManager.AddWall(e1, cell, e2, neighbour, hasRiverThroughEdge, hasRoadThroughEdge);

        HexCell nextNeighbour = cell.GetNeighbour(direction.Next());

        if (direction <= HexDirection.E && nextNeighbour != null) //three cells share one triangular connection so we only need 2 of them
        {
            Vector3 v5 = e1.v5 + HexMetrics.GetBridge(direction.Next());
            v5.y = nextNeighbour.Position.y;

            if (cell.Elevation <= neighbour.Elevation)
            {
                if (cell.Elevation <= nextNeighbour.Elevation)
                {
                    TriangulateCorner(e1.v5, cell, e2.v5, neighbour, v5, nextNeighbour);
                }
                else
                {
                    TriangulateCorner(v5, nextNeighbour, e1.v5, cell, e2.v5, neighbour);
                }
            }
            else if (neighbour.Elevation <= nextNeighbour.Elevation)
            {
                TriangulateCorner(e2.v5, neighbour, v5, nextNeighbour, e1.v5, cell);
            }
            else
            {
                TriangulateCorner(v5, nextNeighbour, e1.v5, cell, e2.v5, neighbour);
            }
        }
    }

    void TriangulateEdgeTerraces(EdgeVertices begin, HexCell beginCell, EdgeVertices end, HexCell endCell, bool hasRoad)
    {
        EdgeVertices e2 = EdgeVertices.TerraceLerp(begin, end, 1);
        Color c2 = HexMetrics.TerraceLerpColour(colour1, colour2, 1);

        float t1 = beginCell.TerrainTypeIndex;
        float t2 = endCell.TerrainTypeIndex;

        TriangulateEdgeStrip(begin, colour1, t1, e2, c2, t2, hasRoad);

        for (int i = 2; i < HexMetrics.terraceSteps; i++)
        {
            EdgeVertices e1 = e2;
            Color c1 = c2;

            e2 = EdgeVertices.TerraceLerp(begin, end, i);
            c2 = HexMetrics.TerraceLerpColour(colour1, colour2, i);

            TriangulateEdgeStrip(e1, c1, t1, e2, c2, t2, hasRoad);
        }

        TriangulateEdgeStrip(e2, c2, t1, end, colour2, t2, hasRoad);
    }

    void TriangulateCorner(Vector3 bottom, HexCell bottomCell,
                            Vector3 left, HexCell leftCell,
                            Vector3 right, HexCell rightCell)
    {
        HexEdgeType leftEdgeType = bottomCell.GetEdgeType(leftCell);
        HexEdgeType rightEdgeType = bottomCell.GetEdgeType(rightCell);

        if (leftEdgeType == HexEdgeType.Slope)
        {
            if (rightEdgeType == HexEdgeType.Slope)
            {
                TriangulateCornerTerraces(bottom, bottomCell, left, leftCell, right, rightCell);
            }
            else if (rightEdgeType == HexEdgeType.Flat)
            {
                TriangulateCornerTerraces(left, leftCell, right, rightCell, bottom, bottomCell);
            }
            else
            {
                TriangulateCornerTerracesCliff(bottom, bottomCell, left, leftCell, right, rightCell);
            }
        }
        else if (rightEdgeType == HexEdgeType.Slope)
        {
            if (leftEdgeType == HexEdgeType.Flat)
            {
                TriangulateCornerTerraces(right, rightCell, bottom, bottomCell, left, leftCell);
            }
            else
            {
                TriangulateCornerCliffTerraces(bottom, bottomCell, left, leftCell, right, rightCell);
            }
        }
        else if (leftCell.GetEdgeType(rightCell) == HexEdgeType.Slope)
        {
            if (leftCell.Elevation < rightCell.Elevation)
            {
                TriangulateCornerCliffTerraces(right, rightCell, bottom, bottomCell, left, leftCell);
            }
            else
            {
                TriangulateCornerTerracesCliff(left, leftCell, right, rightCell, bottom, bottomCell);
            }
        }
        else
        {
            terrain.AddTriangle(bottom, left, right);
            terrain.AddTriangleColour(colour1, colour2, colour3);
            Vector3 types;
            types.x = bottomCell.TerrainTypeIndex;
            types.y = leftCell.TerrainTypeIndex;
            types.z = rightCell.TerrainTypeIndex;
            terrain.AddTriangleTerrainTypes(types);
        }

        featureManager.AddWall(bottom, bottomCell, left, leftCell, right, rightCell);
    }

    void TriangulateCornerTerraces(Vector3 begin, HexCell beginCell, Vector3 left, HexCell leftCell, Vector3 right, HexCell rightCell)
    {
        Vector3 v3 = HexMetrics.TerraceLerp(begin, left, 1);
        Vector3 v4 = HexMetrics.TerraceLerp(begin, right, 1);

        Color c3 = HexMetrics.TerraceLerpColour(colour1, colour2, 1);
        Color c4 = HexMetrics.TerraceLerpColour(colour1, colour3, 1);

        Vector3 types;
        types.x = beginCell.TerrainTypeIndex;
        types.y = leftCell.TerrainTypeIndex;
        types.z = rightCell.TerrainTypeIndex;


        terrain.AddTriangle(begin, v3, v4);
        terrain.AddTriangleColour(colour1, c3, c4);
        terrain.AddTriangleTerrainTypes(types);

        for (int i = 2; i < HexMetrics.terraceSteps; i++)
        {
            Vector3 v1 = v3;
            Vector3 v2 = v4;
            Color c1 = c3;
            Color c2 = c4;

            v3 = HexMetrics.TerraceLerp(begin, left, i);
            v4 = HexMetrics.TerraceLerp(begin, right, i);
            c3 = HexMetrics.TerraceLerpColour(colour1, colour2, i);
            c4 = HexMetrics.TerraceLerpColour(colour1, colour3, i);

            terrain.AddQuad(v1, v2, v3, v4);
            terrain.AddQuadColour(c1, c2, c3, c4);
            terrain.AddQuadTerrainTypes(types);
        }

        terrain.AddQuad(v3, v4, left, right);
        terrain.AddQuadColour(c3, c4, colour2, colour3);
        terrain.AddQuadTerrainTypes(types);
    }

    //Collapse the terraces to a boundary point placed at one elevation level above the bottom cell. 
    //This can be found by interpolating based on elevation difference.
    void TriangulateCornerTerracesCliff(Vector3 begin, HexCell beginCell, Vector3 left, HexCell leftCell, Vector3 right, HexCell rightCell)
    {
        float b = Mathf.Abs(1f / (rightCell.Elevation - beginCell.Elevation));
        Vector3 boundary = Vector3.Lerp(HexMetrics.Perturb(begin), HexMetrics.Perturb(right), b);
        Color boundaryColour = Color.Lerp(colour1, colour3, b);

        Vector3 types;
        types.x = beginCell.TerrainTypeIndex;
        types.y = leftCell.TerrainTypeIndex;
        types.z = rightCell.TerrainTypeIndex;

        TriangulateBoundaryTriangle(begin, colour1, left, colour2, boundary, boundaryColour, types);

        if (leftCell.GetEdgeType(rightCell) == HexEdgeType.Slope)
        {
            TriangulateBoundaryTriangle(left, colour2, right, colour3, boundary, boundaryColour, types);
        }
        else
        {
            terrain.AddTriangleUnperturbed(HexMetrics.Perturb(left), HexMetrics.Perturb(right), boundary);
            terrain.AddTriangleColour(colour2, colour3, boundaryColour);
            terrain.AddTriangleTerrainTypes(types);
        }
    }

    void TriangulateCornerCliffTerraces(Vector3 begin, HexCell beginCell, Vector3 left, HexCell leftCell, Vector3 right, HexCell rightCell)
    {
        float b = Mathf.Abs(1f / (leftCell.Elevation - beginCell.Elevation));
        Vector3 boundary = Vector3.Lerp(HexMetrics.Perturb(begin), HexMetrics.Perturb(left), b);
        Color boundaryColour = Color.Lerp(colour1, colour2, b);

        Vector3 types;
        types.x = beginCell.TerrainTypeIndex;
        types.y = leftCell.TerrainTypeIndex;
        types.z = rightCell.TerrainTypeIndex;

        TriangulateBoundaryTriangle(right, colour3, begin, colour1, boundary, boundaryColour, types);

        if (leftCell.GetEdgeType(rightCell) == HexEdgeType.Slope)
        {
            TriangulateBoundaryTriangle(left, colour2, right, colour3, boundary, boundaryColour, types);
        }
        else
        {
            terrain.AddTriangleUnperturbed(HexMetrics.Perturb(left), HexMetrics.Perturb(right), boundary);
            terrain.AddTriangleColour(colour2, colour3, boundaryColour);
            terrain.AddTriangleTerrainTypes(types);
        }
    }

    void TriangulateBoundaryTriangle(Vector3 begin, Color beginColour, Vector3 left, Color leftColour, Vector3 boundary, Color boundaryColour, Vector3 types)
    {
        Vector3 v2 = HexMetrics.Perturb(HexMetrics.TerraceLerp(begin, left, 1));
        Color c2 = HexMetrics.TerraceLerpColour(beginColour, leftColour, 1);

        terrain.AddTriangleUnperturbed(HexMetrics.Perturb(begin), v2, boundary);
        terrain.AddTriangleColour(beginColour, c2, boundaryColour);
        terrain.AddTriangleTerrainTypes(types);

        for (int i = 2; i < HexMetrics.terraceSteps; i++)
        {
            Vector3 v1 = v2;
            Color c1 = c2;
            v2 = HexMetrics.Perturb(HexMetrics.TerraceLerp(begin, left, i));
            c2 = HexMetrics.TerraceLerpColour(beginColour, leftColour, i);

            terrain.AddTriangleUnperturbed(v1, v2, boundary); //v1 and v2 are perturbed, see above
            terrain.AddTriangleColour(c1, c2, boundaryColour);
            terrain.AddTriangleTerrainTypes(types);
        }

        terrain.AddTriangleUnperturbed(v2, HexMetrics.Perturb(left), boundary); //v2 is perturbed, see above
        terrain.AddTriangleColour(c2, leftColour, boundaryColour);
        terrain.AddTriangleTerrainTypes(types);
    }

    void TriangulateEdgeFan(Vector3 centre, EdgeVertices edge, float type)
    {
        terrain.AddTriangle(centre, edge.v1, edge.v2);
        terrain.AddTriangle(centre, edge.v2, edge.v3);
        terrain.AddTriangle(centre, edge.v3, edge.v4);
        terrain.AddTriangle(centre, edge.v4, edge.v5);

        terrain.AddTriangleColour(colour1);
        terrain.AddTriangleColour(colour1);
        terrain.AddTriangleColour(colour1);
        terrain.AddTriangleColour(colour1);

        Vector3 types;
        types.x = types.y = types.z = type;

        terrain.AddTriangleTerrainTypes(types);
        terrain.AddTriangleTerrainTypes(types);
        terrain.AddTriangleTerrainTypes(types);
        terrain.AddTriangleTerrainTypes(types);
    }

    void TriangulateEdgeStrip(EdgeVertices e1, Color c1, float type1, EdgeVertices e2, Color c2, float type2, bool hasRoad = false)
    {
        terrain.AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
        terrain.AddQuad(e1.v2, e1.v3, e2.v2, e2.v3);
        terrain.AddQuad(e1.v3, e1.v4, e2.v3, e2.v4);
        terrain.AddQuad(e1.v4, e1.v5, e2.v4, e2.v5);

        terrain.AddQuadColour(c1, c2);
        terrain.AddQuadColour(c1, c2);
        terrain.AddQuadColour(c1, c2);
        terrain.AddQuadColour(c1, c2);

        Vector3 types;
        types.x = types.z = type1;
        types.y = type2;
        terrain.AddQuadTerrainTypes(types);
        terrain.AddQuadTerrainTypes(types);
        terrain.AddQuadTerrainTypes(types);
        terrain.AddQuadTerrainTypes(types);

        if (hasRoad)
        {
            TriangulateRoadSegment(e1.v2, e1.v3, e1.v4, e2.v2, e2.v3, e2.v4);
        }
    }

    void TriangulateRiverQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, float y1, float y2, float v, bool reversed)
    {
        v1.y = v2.y = y1;
        v3.y = v4.y = y2;

        rivers.AddQuad(v1, v2, v3, v4);
        if (reversed)
        {
            rivers.AddQuadUV(1f, 0f, 0.8f - v, 0.6f - v);
        }
        else
        {
            rivers.AddQuadUV(0f, 1f, v, v + 0.2f);
        }
    }

    void TriangulateRiverQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, float y, float v, bool reversed)
    {
        TriangulateRiverQuad(v1, v2, v3, v4, y, y, v, reversed);
    }

    void TriangulateRoadSegment(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, Vector3 v5, Vector3 v6)
    {
        roads.AddQuad(v1, v2, v4, v5);
        roads.AddQuad(v2, v3, v5, v6);
        roads.AddQuadUV(0f, 1f, 0f, 0f);
        roads.AddQuadUV(1f, 0f, 0f, 0f);
    }

    void TriangulateRoad(Vector3 centre, Vector3 mL, Vector3 mR, EdgeVertices e, bool hasRoadThroughCellEdge)
    {
        if (hasRoadThroughCellEdge)
        {
            Vector3 mC = Vector3.Lerp(mL, mR, 0.5f);
            TriangulateRoadSegment(mL, mC, mR, e.v2, e.v3, e.v4);

            roads.AddTriangle(centre, mL, mC);
            roads.AddTriangle(centre, mC, mR);

            roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(0f, 0f), new Vector2(1f, 0f));
            roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(1f, 0f), new Vector2(0f, 0f));
        }
        else
        {
            TriangulateRoadEdge(centre, mL, mR);
        }
    }

    void TriangulateRoadEdge(Vector3 centre, Vector3 mL, Vector3 mR)
    {
        roads.AddTriangle(centre, mL, mR);
        roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(0f, 0f), new Vector2(0f, 0f));
    }

    Vector2 GetRoadInterpolators(HexDirection direction, HexCell cell)
    {
        Vector2 interpolators;
        if(cell.HasRoadThroughEdge(direction))
        {
            interpolators.x = interpolators.y = 0.5f;
        }
        else
        {
            interpolators.x = cell.HasRoadThroughEdge(direction.Previous()) ? 0.5f : 0.25f;
            interpolators.y = cell.HasRoadThroughEdge(direction.Next()) ? 0.5f : 0.25f;
        }

        return interpolators;
    }

    void TriangulateWaterfallInWater(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, float y1, float y2, float waterY)
    {
        v1.y = v2.y = y1;
        v3.y = v4.y = y2;

        v1 = HexMetrics.Perturb(v1);
        v2 = HexMetrics.Perturb(v2);
        v3 = HexMetrics.Perturb(v3);
        v4 = HexMetrics.Perturb(v4);

        float t = (waterY - y2) / (y1 - y2);
        v3 = Vector3.Lerp(v3, v1, t);
        v4 = Vector3.Lerp(v4, v2, t);
        rivers.AddQuadUnperturbed(v1, v2, v3, v4); // have to manually perturb the vertices (see above) because the original positions have changed
        rivers.AddQuadUV(0f, 1f, 0.8f, 1f);
    }

}
