﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoBox : MonoBehaviour {
    public Text unitInfoBox;

    public void UpdateInfoBox(HexUnit unit, bool fromMenu = false)
    {
        UnitPower ability = unit.GetComponentInChildren<UnitPower>();

        string healthText = fromMenu ? ("\nMax Health: " + unit.maxHealth) : ("\nCurrent Health: " + unit.Health);
        string budgetText = fromMenu ? ("\nMax move budget: " + unit.MoveBudgetMax) : ("\nCurrent Move budget: " + unit.MoveBudget);

        unitInfoBox.text = "Unit type: " + unit.unitType +
                            healthText +
                            "\nCan travel over water:" + unit.CanTravelOverWater +
                            "\nAttack range: " + ability.range +
                            "\nDamage: " + ability.damage +
                            "\nType of damage: " + ability.typeOfDamage +
                            "\nAttack Cost: " + ability.cost + 
                            budgetText;

        if (ability.causeStatusEffectResistance)
        {
            unitInfoBox.text += "\nCause resistance to: ";
            foreach (var resistance in unit.Resistances)
            {
                unitInfoBox.text += resistance.ToString() + "\n";
            }
        }

        if (ability.causeStatusEffectVulnerability)
        {
            unitInfoBox.text += "\nCause vulnerability to: ";
            foreach (var vulnerability in unit.Vulnerabilities)
            {
                unitInfoBox.text += vulnerability.ToString() + "\n";
            }
        }
    }
}
