﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

public class UnitButton : MonoBehaviour, IPointerEnterHandler
{
    //Use this to check what Events are happening
    //public Button button;
    public InfoBox infoBox;
    public HexUnit unit;

    public void OnPointerEnter(PointerEventData eventData)
    {
        infoBox.transform.parent.gameObject.SetActive(true);
        infoBox.UpdateInfoBox(unit, true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        infoBox.transform.parent.gameObject.SetActive(false);
    }
}
