﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Bezier
{
    //quadratic bezier curve
    //(1-t)*(1-t)*A + 2(1-t)*t*B + t*t*C
    //A,B,C are control points
    //t is interpolator
    public static Vector3 GetPoint(Vector3 a, Vector3 b, Vector3 c, float t)
    {
        float r = 1f - t;
        return (r * r * a + 2f * r * t * b + t * t * c);
    }

    //no need to clamp to [0,1] as the interpolator passed in is already clamped

    public static Vector3 GetDerivative(Vector3 a, Vector3 b, Vector3 c, float t) //used to find the orientation/ direction of movement
    {// Derivative = 2 * ((1-t)(B-A) + t * (C-B))
        return (2f * ((1f - t) * (b - a) + t * (c - b)));
    }
}
