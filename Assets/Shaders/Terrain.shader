﻿Shader "Custom/Terrain" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Terrain Texture Array", 2DArray) = "white" {}
		_GridTex("Grid Texture", 2D) = "white"{}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.5

		#pragma multi_compile _ GRID_ON

		UNITY_DECLARE_TEX2DARRAY(_MainTex);

		struct Input {
			float4 colour : color;
			float3 worldPos;
			float3 terrain;
		};

		void vert(inout appdata_full v, out Input data)
		{
			UNITY_INITIALIZE_OUTPUT(Input, data);
			data.terrain = v.texcoord2.xyz;
		}

		float4 GetTerrainColour(Input IN, int index)
		{
			float3 uvw = float3(IN.worldPos.xz * 0.02, IN.terrain[index]);
			float4 c = UNITY_SAMPLE_TEX2DARRAY(_MainTex, uvw);
			return c*IN.colour[index];
		}

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		sampler2D _GridTex;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = GetTerrainColour(IN, 0) + GetTerrainColour(IN, 1) + GetTerrainColour(IN, 2);

			//We have to scale the pattern so it fits the cells of our map. 
			//The forward distance between adjacent cell centers is 15, twice that to move two cells straight up. 
			//So we have to divide our grid's V coordinates by 30. 
			//And the inner radius of our cells is 5√3, so four times that is needed to move two cells to the right. 
			//Thus we have to divide the grid's U coordinates by 20√3.
			fixed4 grid = 1;
#if defined(GRID_ON) //grid toggleable via the panel

			float2 gridUV = IN.worldPos.xz;
			gridUV.x *= 1 / (4 * 8.66025404);
			gridUV.y *= 1 / (2 * 15.0);
			
			grid = tex2D(_GridTex, gridUV);
#endif
			o.Albedo = c.rgb * _Color * grid;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
